/*
    FreeRTOS V8.2.1 - Copyright (C) 2015 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.

    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation >>!AND MODIFIED BY!<< the FreeRTOS exception.

    ***************************************************************************
    >>!   NOTE: The modification to the GPL is included to allow you to     !<<
    >>!   distribute a combined work that includes FreeRTOS without being   !<<
    >>!   obliged to provide the source code for proprietary components     !<<
    >>!   outside of the FreeRTOS kernel.                                   !<<
    ***************************************************************************

    FreeRTOS is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  Full license text is available on the following
    link: http://www.freertos.org/a00114.html

    ***************************************************************************
     *                                                                       *
     *    FreeRTOS provides completely free yet professionally developed,    *
     *    robust, strictly quality controlled, supported, and cross          *
     *    platform software that is more than just the market leader, it     *
     *    is the industry's de facto standard.                               *
     *                                                                       *
     *    Help yourself get started quickly while simultaneously helping     *
     *    to support the FreeRTOS project by purchasing a FreeRTOS           *
     *    tutorial book, reference manual, or both:                          *
     *    http://www.FreeRTOS.org/Documentation                              *
     *                                                                       *
    ***************************************************************************

    http://www.FreeRTOS.org/FAQHelp.html - Having a problem?  Start by reading
    the FAQ page "My application does not run, what could be wrong?".  Have you
    defined configASSERT()?

    http://www.FreeRTOS.org/support - In return for receiving this top quality
    embedded software for free we request you assist our global community by
    participating in the support forum.

    http://www.FreeRTOS.org/training - Investing in training allows your team to
    be as productive as possible as early as possible.  Now you can receive
    FreeRTOS training directly from Richard Barry, CEO of Real Time Engineers
    Ltd, and the world's leading authority on the world's leading RTOS.

    http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
    including FreeRTOS+Trace - an indispensable productivity tool, a DOS
    compatible FAT file system, and our tiny thread aware UDP/IP stack.

    http://www.FreeRTOS.org/labs - Where new FreeRTOS products go to incubate.
    Come and try FreeRTOS+TCP, our new open source TCP/IP stack for FreeRTOS.

    http://www.OpenRTOS.com - Real Time Engineers ltd. license FreeRTOS to High
    Integrity Systems ltd. to sell under the OpenRTOS brand.  Low cost OpenRTOS
    licenses offer ticketed support, indemnification and commercial middleware.

    http://www.SafeRTOS.com - High Integrity Systems also provide a safety
    engineered and independently SIL3 certified version for use in safety and
    mission critical applications that require provable dependability.

    1 tab == 4 spaces!
*/

/******************************************************************************
 * This project provides two demo applications.  A simple blinky style project,
 * and a more comprehensive test and demo application.  The
 * mainCREATE_SIMPLE_BLINKY_DEMO_ONLY setting (defined in this file) is used to
 * select between the two.  The simply blinky demo is implemented and described
 * in main_blinky.c.  The more comprehensive test and demo application is
 * implemented and described in main_full.c.
 *
 * This file implements the code that is not demo specific, including the
 * hardware setup and FreeRTOS hook functions.
 */

/* Hardware specific includes. */
#include "ConfigPerformance.h"

/* Core configuratin fuse settings */
#include "bitsConfig.h"

/* Includes de la app */
#include "ArtLogSD.h"
#include "serial.h"

/*-----------------------------------------------------------*/
/*
 * Set up the hardware ready to run this demo.
 */
static void prvSetupHardware( void );

/*
 * Create the demo tasks then start the scheduler.
 */
int main( void )
{
	/* Prepare the hardware to run this demo. */
	prvSetupHardware();
    
    /* --- APPLICATION TASKS CAN BE CREATED HERE --- */
    
    /* Tiempo para estabilización de voltaje de entrada */   
    delay_ms(1500);      /* 1.5 seg */
  
    /* Configuración de UART */
    vSerialPortInit();
    
    /* Tiempo para estabilización de UART */
    delay_ms(500);      /* 0.5 seg */
    
        vToggleLED(SD_LED);
        vToggleLED(RX_LED);
    
    /* Configuración de ADC */
    vADCPortInit();
    
    /* Tiempo para estabilización de ADC */
    delay_ms(500);      /* 0.5 seg */
     
    /* Mensaje de Entrada */
    printf( "ARTLOGSD V%1.1f \n", FWVersion );
    
        vToggleLED(SD_LED);
        vToggleLED(RX_LED);
             
    /* Creación de Queue */
    xStatusLED              = xQueueCreate( QUEUE_LENGTH_STATUSLED,   sizeof( uint8_t * ) );
    xRxedChars              = xQueueCreate( QUEUE_LENGTH_RXCHARS,     sizeof( uint8_t * ) );
    xRxedCharsAux           = xQueueCreate( QUEUE_LENGTH_RXCHARSAUX,  sizeof( uint8_t * ) );
    xMensajeAutoBaudRate    = xQueueCreate( QUEUE_LENGTH_MSNAUTOBR,   sizeof( uint8_t * ) );
    
    /* Creación de Task */
    xTaskCreate( ABaudRTask, "ABAUDR",      STACK_SIZE,     NULL,   ABAUDR_PRIORITY,    &pxABaudRTask );
    xTaskCreate( InitSDTask, "INITSD",      STACK_SIZE,     NULL,   INITSD_PRIORITY,    &pxInitSDTask );
    xTaskCreate( WriteSDTask,"WRTSD",       STACK_SIZE,     NULL,   WRTSD_PRIORITY,     &pxWriteSDTask);
    xTaskCreate( CountFData, "COUNTFDATA",  STACK_SIZE,     NULL,   COUNTDATA_PRIORITY, &pxCountFData );
    xTaskCreate( FreeData,   "FREEDATA",    STACK_SIZE,     NULL,   FREDATA_PRIORITY,   &pxFreeData   );
    xTaskCreate( LedTask,    "LEDS",        STACK_SIZE,     NULL,   LEDS_PRIORITY,      &pxLedTask    );
    xTaskCreate( ADCTask,    "ADC24_12",    STACK_SIZE,     NULL,   ADC_PRIORITY,       &pxADCTask    );

    /* Creación de Semaphores */
    xFlagFreeData = xSemaphoreCreateBinary();
       
    /* Creación de Timer para guardar datos menores a 2KB */
    xTimerOutFreeD = xTimerCreate( "TIMEROUT", TIMER_OUT_PERIOD, pdTRUE, ( void * ) 0, TimeOutFreeD );
    
    if( xTimerOutFreeD != NULL )
    { 
        xTimerStart( xTimerOutFreeD, 0 ); 
    }
    
    /* Creación de Timer para eliminar ruido del switch de SD Card */
    xTimerOutCD_SW_1 = xTimerCreate( "TIMEROUT_SW", TIMER_OUT_PERIOD, pdTRUE, ( void * ) 0, TimeOutCD_SW_1 );
    
    if( xTimerOutCD_SW_1 != NULL )
    { 
        xTimerStart( xTimerOutCD_SW_1, 0 ); 
    }
    
    /* Creación de Timer para eliminar ruido del switch de SD Card */
    xTimerOutCD_SW_2 = xTimerCreate( "TIMEROUT_SW", TIMER_OUT_PERIOD, pdTRUE, ( void * ) 0, TimeOutCD_SW_2 );
    
    if( xTimerOutCD_SW_2 != NULL )
    { 
        xTimerStart( xTimerOutCD_SW_2, 0 ); 
    }
    
    /* Creación de Timer para parpadeo de LEDs */
    xTimerOutLED = xTimerCreate( "TIMEROUT_LED", TIMER_OUT_LED, pdTRUE, ( void * ) 0, TimeOutLED );
    
    if( xTimerOutLED != NULL )
    { 
        xTimerStart( xTimerOutLED, 0 ); 
    }
    
    /* Creación de Timer guardar valor de Vin en SD Card */
    xTimerOutVin = xTimerCreate( "TIMEROUT_Vin", TIMER_OUT_Vin, pdTRUE, ( void * ) 0, TimeOutVin );
    
    if( xTimerOutVin != NULL )
    { 
        xTimerStart( xTimerOutVin, 0 ); 
    }
            
    /* Start the created tasks running */
    vTaskStartScheduler();
    
    /* Execution will only reach here if there was insufficient heap to 
     start he scheduler */
    for(;;);
    
	return 0;
}
/*-----------------------------------------------------------*/

static void prvSetupHardware( void )
{
	/* Configure the hardware for maximum performance. */
	vHardwareConfigurePerformance();

	/* Setup to use the external interrupt controller. */
	vHardwareUseMultiVectoredInterrupts();

	portDISABLE_INTERRUPTS();

	/* Configuración de Hardware */
    vHWInicializacion();
}
/*-----------------------------------------------------------*/

void vApplicationMallocFailedHook( void )
{
	/* vApplicationMallocFailedHook() will only be called if
	configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h.  It is a hook
	function that will get called if a call to pvPortMalloc() fails.
	pvPortMalloc() is called internally by the kernel whenever a task, queue,
	timer or semaphore is created.  It is also called by various parts of the
	demo application.  If heap_1.c or heap_2.c are used, then the size of the
	heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
	FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
	to query the size of free heap space that remains (although it does not
	provide information on how the remaining heap might be fragmented). */
	taskDISABLE_INTERRUPTS();
	for( ;; );
}
/*-----------------------------------------------------------*/

void vApplicationIdleHook( void )
{
	/* vApplicationIdleHook() will only be called if configUSE_IDLE_HOOK is set
	to 1 in FreeRTOSConfig.h.  It will be called on each iteration of the idle
	task.  It is essential that code added to this hook function never attempts
	to block in any way (for example, call xQueueReceive() with a block time
	specified, or call vTaskDelay()).  If the application makes use of the
	vTaskDelete() API function (as this demo application does) then it is also
	important that vApplicationIdleHook() is permitted to return to its calling
	function, because it is the responsibility of the idle task to clean up
	memory allocated by the kernel to any task that has since been deleted. */
}
/*-----------------------------------------------------------*/

void vApplicationStackOverflowHook( TaskHandle_t pxTask, char *pcTaskName )
{
	( void ) pcTaskName;
	( void ) pxTask;

	/* Run time task stack overflow checking is performed if
	configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.  This hook	function is 
	called if a task stack overflow is detected.  Note the system/interrupt
	stack is not checked. */
	taskDISABLE_INTERRUPTS();
	for( ;; );
}
/*-----------------------------------------------------------*/

void vApplicationTickHook( void )
{
	/* This function will be called by each tick interrupt if
	configUSE_TICK_HOOK is set to 1 in FreeRTOSConfig.h.  User code can be
	added here, but the tick hook is called from an interrupt context, so
	code must not attempt to block, and only the interrupt safe FreeRTOS API
	functions can be used (those that end in FromISR()). */
}
/*-----------------------------------------------------------*/

/* Exception handler: */
static enum {
    EXCEP_IRQ = 0,                  // interrupt
    EXCEP_AdEL = 4,                 // address error exception (load or ifetch)
    EXCEP_AdES,                     // address error exception (store)
    EXCEP_IBE,                      // bus error (ifetch)
    EXCEP_DBE,                      // bus error (load/store)
    EXCEP_Sys,                      // syscall
    EXCEP_Bp,                       // breakpoint
    EXCEP_RI,                       // reserved instruction
    EXCEP_CpU,                      // coprocessor unusable
    EXCEP_Overflow,                 // arithmetic overflow
    EXCEP_Trap,                     // trap (possible divide by zero)
    EXCEP_IS1 = 16,                 // implementation specfic 1
    EXCEP_CEU,                      // CorExtend Unuseable
    EXCEP_C2E                       // coprocessor 2
} _excep_code;

static unsigned int _epc_code;
static unsigned int _excep_addr;

void _general_exception_handler( unsigned long ulCause, unsigned long ulStatus )
{
	/* This overrides the definition provided by the kernel.  Other exceptions 
	should be handled here. */
    
    /* Se elimina warning del compilador */
    ( void ) ulCause;
    ( void ) ulStatus;
    
    uint32_t counter = 0;
    
    asm volatile( "mfc0 %0,$13" : "=r" (_excep_code) );
    asm volatile( "mfc0 %0,$14" : "=r" (_excep_addr) );
  
    _excep_code = (_excep_code & 0x0000007C) >> 2;
    
    printf( "\n***************************************************************\n" );
    printf( "General Exception: " );
    
    switch( _excep_code )
    {
        case EXCEP_IRQ:     printf( "interrupt" );break;
        case EXCEP_AdEL:    printf( "address error exception (load or ifetch)" );break;
        case EXCEP_AdES:    printf( "address error exception (store)" );break;
        case EXCEP_IBE:     printf( "bus error (ifetch)" );break;
        case EXCEP_DBE:     printf( "bus error (load/store)" );break;
        case EXCEP_Sys:     printf( "syscall" );break;
        case EXCEP_Bp:      printf( "breakpoint" );break;
        case EXCEP_RI:      printf( "reserved instruction" );break;
        case EXCEP_CpU:     printf( "coprocessor unusable" );break;
        case EXCEP_Overflow:    printf( "arithmetic overflow" );break;
        case EXCEP_Trap:    printf( "trap (possible divide by zero)" );break;
        case EXCEP_IS1:     printf( "implementation specfic 1" );break;
        case EXCEP_CEU:     printf( "CorExtend Unuseable" );break;
        case EXCEP_C2E:     printf( "coprocessor 2" );break;
      }
    
    printf( " at 0x%u \n",_excep_addr );

	for( ;; )
    {    
        delay_ms(2000);      /* 2 seg */
        
        vToggleLED( SD_LED );
        vToggleLED( RX_LED );
        
        counter++;
        if( counter > 25 )
        {
            printf( "SOFT_RESET \n" );
            printf( "***************************************************************\n" );
            SoftReset();
        }
    }
}
/*-----------------------------------------------------------*/

void vAssertCalled( const char * pcFile, unsigned long ulLine )
{
volatile unsigned long ul = 0;

	( void ) pcFile;
	( void ) ulLine;

	__asm volatile( "di" );
	{
		/* Set ul to a non-zero value using the debugger to step out of this
		function. */
		while( ul == 0 )
		{
			portNOP();
		}
	}
	__asm volatile( "ei" );
}