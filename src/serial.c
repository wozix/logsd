/* Archivo:         serial.c
 * Autor:           Olaf I. Gómez P.
 * Fecha:           22/Octubre/15
 * uC:              PIC32MX170F256B
 * Versión:         0
 * Descripción:     Configuración de RS232, Interrupciones, Task, etc.
 *                    
 */

/* FreeRTOS includes */
#include "FreeRTOS.h"

/* App Include */
#include "ArtLogSD.h"
#include "serial.h"
    
uint32_t cadeCount;      /* Contador de cadenas */

static volatile uint32_t Timer3;
static bool charCR_OK;

/* La secuencia transmitida debe terminar con los caracteres <LF> <CR> */
#define serLAST_BYTE_LF             ( 0x0A )  /* Salto de linea <LF> */
#define serLAST_BYTE_CR             ( 0x0D )  /* Retorno de carro <CR> */
        
/* -------------------------------------------------------------------------- */
/* Manejador de la interrupcion de UART. IPL0AUTO no tiene efecto, ya que es 
 * usado por FreeRTOS. 
 * La prioridad se configura con el llamado a ConfigIntUART1. */
void __attribute__( (interrupt(IPL0AUTO), vector(_UARTx_VECTOR))) vUxInterruptWrapper( void );

/* -------------------------------------------------------------------------- */
/* Nombre de Función:   vSerialPortInit
 * Objetivo:            Configuración de UART. */
void vSerialPortInit( void )
{
    /* Cambio de UART para funcion printf */
    __XC_UART = 1;
    
    /* Configuración de UART */
    UARTConfigure( UARTx, config_flags );
    UARTSetFifoMode( UARTx, config_FIFO_Mode ); 
    UARTSetLineControl( UARTx, config_Line_Control_Mode );
    UARTSetDataRate( UARTx, PERIPHERAL_CLOCK, COM_BAUD_RATE );
    UARTEnable( UARTx, UART_ENABLE_FLAGS( UART_PERIPHERAL | UART_RX | UART_TX ) );
    
    /* Configuración de Interrupciones */
    INTEnable( INT_SOURCE_UART_RX( UARTx ), INT_DISABLED );
    INTEnable( INT_SOURCE_UART_TX( UARTx ), INT_DISABLED );
    INTSetVectorPriority( INT_VECTOR_UART( UARTx ), INT_PRIORITY_LEVEL_3 );
    INTSetVectorSubPriority( INT_VECTOR_UART( UARTx ), INT_SUB_PRIORITY_LEVEL_0 );
 
    /* Configuración en modo multivector */
    INTConfigureSystem( INT_SYSTEM_CONFIG_MULT_VECTOR );

    /* Habilitación de Interrupciones */
    INTEnableInterrupts(); 
       
    /* Inicialización */
    iChar = 0;
    cntChar_BaudRate = 0;
    cntFraming_Error = 0;
    index_baudRates = 0;
    flag_autoBaudRate = FALSE;
    charCR_OK = FALSE;
    cadeCount = 0;
    
    Timer3 = 60000;          /* 1 min */
}

/* -------------------------------------------------------------------------- */
/* Interrupción:        UART
 * Objetivo:            Código que se debe ejecutar al ocurrir la interrupción
 *                      del UART. */
void vUxInterruptHandler( void )
{    
    /* Declaraciones */
    uint8_t cChar;
    static portBASE_TYPE xHigherPriorityTaskWoken;
    
	xHigherPriorityTaskWoken = pdFALSE;

	/* ¿Interrupción por Rx? */
    if( INTGetFlag( INT_UxRX ) )
    {        
        /* Identificación de Framing Error */
        if( U1ASTAbits.FERR == 1 )
        {
            printf( "Framing Error...\n" );
            /* iChar = 0; /* NO se debe limpiar indice, pueden perderse datos */
            cntFraming_Error++;
            
//            if( cntFraming_Error >= 20 ) 
//            {
//            
//                cntFraming_Error = 0;
//                printf( "Buad-Rate Incorrecto \n");	
//                UARTSetDataRate( UARTx, PERIPHERAL_CLOCK, baudRates[index_baudRates] );   
//                index_baudRates++;
//                
//                /* Reinicio de indice de busqueda de Baud-Rate */
//                if( index_baudRates >= sizeof(baudRates) ) index_baudRates = 0;  
//            }           
            
        }
        
        /* Identificación de Overrun */
        if( U1ASTAbits.OERR == 1 )
        {
            printf( "Error de Overrun...\n" );
            U1ASTAbits.OERR = 0;
            /* iChar = 0; /* NO se debe limpiar indice, pueden perderse datos */
        }
        
        /* Se recibe caracter de UART y evitar Overrun cuando no hay SD Card */
        cChar = UARTGetDataByte( UARTx );
                
        /* Se guardan datos en presencia de la SD Card */
        if( !CD_SW  )
        {
            /* Se eliminan caracteres NULOS en recepción de datos */
            if( ( cChar > 0x00 ) & ( cChar <= 127 ) )
            {
                /* Se almacena el char en una fila temporal */
                xRxedCharsPar[iChar] = cChar;

                iChar++;
            }

            /* Prueba para escribir buffer de x Bytes */
            if( iChar >= 2000 ) 
            {
                /* Se evalua fin de cadena */
                if ( cChar == serLAST_BYTE_LF )
                {
                    /* Se limpia Bandera */
                    charCR_OK = FALSE;
                    
                    /* Se cambia \n por \r\n (Formato de Windows) */
                    /* Se considera el caso cuando se recibe \r\n */   
                    if ( xRxedCharsPar[iChar-2] == serLAST_BYTE_CR )
                    {
//                        xRxedCharsPar[iChar-2] = 0x0D;     /* Retorno de carro <CR> */
//                        xRxedCharsPar[iChar-1] = 0x0A;     /* Salto de linea <LF> */
                        xRxedCharsPar[iChar]   = 0x00;     /* Fin de cadena <NULL> */

                    }
                    else
                    {
                        xRxedCharsPar[iChar-1] = 0x0D;     /* Retorno de carro <CR> */
                        xRxedCharsPar[iChar]   = 0x0A;     /* Salto de linea <LF> */
                        iChar++;
                        xRxedCharsPar[iChar]   = 0x00;     /* Fin de cadena <NULL> */
                    }

                    memcpy(xRxedCharsParAUX1, xRxedCharsPar, iChar + 1 );
                    if( !( xTimerPendFunctionCallFromISR(vProcessQueue, (void  *) xRxedCharsParAUX1 , iChar + 1, &xHigherPriorityTaskWoken) ) )
                    {   
                        printf("Error xTimerPend [xRxedCharsParAUX1]... **************************\n");
                    }            
                    iChar = 0;      /* Se limpia contador */    
                }
                else if ( cChar == serLAST_BYTE_CR )
                {
                    /* Se registra esta bandera para indicar que se tiene el caracter CR */
                    charCR_OK = TRUE;
                }
                else if ( TRUE == charCR_OK )
                {
                    /* Se limpia Bandera */
                    charCR_OK = FALSE;
                    
                    xRxedCharsPar[iChar-1] = 0x0D;     /* Retorno de carro <CR> */
                    xRxedCharsPar[iChar]   = 0x0A;     /* Salto de linea <LF> */
                    iChar++;
                    xRxedCharsPar[iChar]   = 0x00;     /* Fin de cadena <NULL> */
                    
                    memcpy(xRxedCharsParAUX1, xRxedCharsPar, iChar + 1 );
                    if( !( xTimerPendFunctionCallFromISR(vProcessQueue, (void  *) xRxedCharsParAUX1 , iChar + 1, &xHigherPriorityTaskWoken) ) )
                    {   
                        printf("Error xTimerPend [xRxedCharsParAUX1]... **************************\n");
                    }            
                    iChar = 0;      /* Se limpia contador */
                    
                    /* Se guarda el ultimo caracter diferente de LF */
                    xRxedCharsPar[iChar] = cChar;
                    iChar++;
                } 
            }

            /* Se entrega semaphore para verificar si faltaran datos por enviar */
            xSemaphoreGiveFromISR( xFlagFreeData, &xHigherPriorityTaskWoken );
        }
        else
        {
            printf( "NO esta presente la SD Card \n" );
        }
        
        /* Limpiar bandera por recepción */
        INTClearFlag( INT_UxRX ); 
    }
    
    /* ¿Interrupción por Tx? */ 
    if( INTGetFlag( INT_UxTX ) )
    {
        /* Limpiar bandera por transmisión */
        INTClearFlag( INT_UxTX ); 
    }
   
    /* If sending or receiving necessitates a context switch, then switch now. */
    portEND_SWITCHING_ISR( xHigherPriorityTaskWoken );
}

/* -------------------------------------------------------------------------- */
/* Nombre de la Función:    vProcessQueue
 * Objetivo:                Pending Funtion para enviar datos recibidos al 
 *                          Queue
 * Observaciones:        */
void vProcessQueue( void  *pvParameter1, uint32_t ulParameter2 )
{    
    /* Declaracion de Variables */
    uint32_t numC;
    uint8_t dataStatusLED;
    uint8_t *datosC;    
    
    /* Inicialización de Variables */
    numC = ulParameter2;
    
    /* Se verifica tamaño de Heap para checar si hay espacio */
    if( xPortGetFreeHeapSize() >= 1000 )
    {
        datosC = pvPortMalloc(numC);
        memcpy( datosC, (uint8_t  *) pvParameter1, numC);
              
        if( xQueueSendToBack( xRxedChars, &datosC, NOT_WAIT_TIME )== pdFALSE )
        {
            printf("Error al Escribir en xRxedChars ***************************************** \n");
            vPortFree(datosC);
        }      
    }
    
    /* Se coloca dato para envío de estado de LEDs */
    dataStatusLED = 0xC0;
    xQueueSendToBack(xStatusLED, &dataStatusLED, NOT_WAIT_TIME);

    /* Se imprime valor de HEAP */
    printf("[vProcessQueue] Free Heap: %u\n", xPortGetFreeHeapSize());
    
}

/* -------------------------------------------------------------------------- */
/* Nombre de la Función:    vProcessMensajeBaudRate
 * Objetivo:                Pending Funtion para envio de mensaje de Auto Baud 
 *                          Rate
 * Observaciones:        */
void vProcessMensajeBaudRate( void  *pvParameter1, uint32_t ulParameter2 ) 
{
    /* Declaracion de Variables */
    uint32_t numC;
    uint8_t *datosC;    
    uint8_t i;
    
    /* Inicialización de Variables */
    numC = ulParameter2;
     
    /* Se verifica tamaño de Heap para checar si hay espacio */
    if( xPortGetFreeHeapSize() >= 1000 )
    {
        datosC = pvPortMalloc( numC + 1 );
        memcpy( datosC, (uint8_t  *) pvParameter1, numC);
        datosC[ numC ] = 0;
        
//        for( i = 0; i <= numC; i++ )
//        {
//            printf("%c",datosC[ i ]);
//        }
        
        if( xQueueSendToBack( xMensajeAutoBaudRate, &datosC, NOT_WAIT_TIME )== pdFALSE )
        {
            printf("Error al Escribir en xMensajeAutoBaudRate ***************************************** \n");
            vPortFree(datosC);
        }      
    }
   
    /* Se imprime valor de HEAP */
    printf("[vProcessMensajeBaudRate] Free Heap: %u\n", xPortGetFreeHeapSize()); 
}

/* -------------------------------------------------------------------------- */
/* Nombre de la Tarea:  CountFData
 * Objetivo:            Determina en que momento se debe Liberar la información 
 *                      de la interrupción que no se ha guardado
 * Observaciones:        */
void CountFData ( void *pvParameters )
{
    /* Solo para no tener warnings */
    ( void ) pvParameters;
    
    /* Definición de Variables  */
    uint8_t *datosC;
    
    /* Limpiar semaphore */
    xSemaphoreTake( xFlagFreeData, NOT_WAIT_TIME );
    
    for( ;; )
    {
        
        vTaskDelay( 10 / portTICK_PERIOD_MS );
        
        /* Se tiene el mensaje */
        if( xSemaphoreTake( xFlagFreeData, NOT_WAIT_TIME) )
        {
            /* Contador de tiempo se reinicia */
            Timer3 = 2000;         /* 2 seg */
        } 
        
        if( Timer3 == 0 )
        {   
            /* Se asigna memoria dinámica para liberar datos */
            datosC = pvPortMalloc(iChar + 1);
            memcpy( datosC, (uint8_t  *) xRxedCharsPar, iChar);
            datosC[iChar] = 0;
            
            /* Se manda a Queue para su liberación */
            if( xQueueSendToBack( xRxedCharsAux, &datosC, NOT_WAIT_TIME )== pdFALSE )
            {
                printf("[ CountFData  ] Error al Escribir en xRxedCharsAux ***************************************** \n");
                vPortFree(datosC);
            }
            
            /* Se limpian valores */
            iChar = 0;       
            
            /* Se reinicia conteo */
            Timer3 = 60000;         /* 1 min */
        }
    }
}
/* -------------------------------------------------------------------------- */
/* Nombre de la Función:    TimeOutFreeD
 * Objetivo:                Timer para liberar datos
 * Observaciones:           */
void TimeOutFreeD( void *pvParameters )
{
    /* Evita warnings del compilador */
    (void) pvParameters;
    
    /* Definición de Variables */
    uint32_t n;

	n = Timer3;					
	if (n) Timer3 = --n;
}
