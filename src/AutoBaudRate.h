/* Archivo:         AutoBaudRate.h
 * Autor:           Olaf I. G�mez P.
 * Fecha:           15/Febrero/16
 * uC:              PIC32MX170F256B
 * Versi�n:         0
 * Descripci�n:     Librer�a para Auto Baud Rate
 *                    
 */

#ifndef AUTOBAUDRATE_H
#define	AUTOBAUDRATE_H

/* Kernel Includes. */
#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"

/* Etiquetas */
/* -------------------------------------------------------------------------- */
#define CHAR_COMA             	( 0x2C )  /* Salto de linea <LF> */
#define CHAR_A_MAYUSCULA      	( 0x41 )  /* Caracter A */
#define CHAR_A_MINUSCULA      	( 0x61 )  /* Caracter a */
#define CHAR_F_MAYUSCULA      	( 0x46 )  /* Caracter F */
#define CHAR_F_MINUSCULA      	( 0x66 )  /* Caracter f */
#define CHAR_T_MAYUSCULA      	( 0x54 )  /* Caracter T */
#define CHAR_T_MINUSCULA      	( 0x74 )  /* Caracter t */
#define CHAR_NUMERO_0         	( 0x30 )  /* N�mero 0 */
#define CHAR_NUMERO_1         	( 0x31 )  /* N�mero 1 */
#define CHAR_NUMERO_9			( 0x39 )  /* N�mero 9 */
#define ETIQUETA_AUTO			"auto"
#define ETIQUETA_FALSE			"false"
#define ETIQUETA_TRUE			"true"

/* Periodo de espera igual a 300 ms */
#define WAIT_TIME_300_ms                ( 1000 / portTICK_PERIOD_MS  )

/******************************************************************************/
/* Define de NVM */
#define NVM_PROGRAM_PAGE                ( 0xBD030000UL )    /* Direcci�n de Inicio de escritura en NVM en 192KB */
//#define NVM_PAGE_SIZE                   1024                /* Tama�o de P�gina */
#define MAGIC_WORD                      ( 0x12345678UL )

//uint32_t pagebuff[ 256 ];                                   /* Instrucciones por Page */
uint32_t databuff_Pagina1[ 32 ];                            /* Instrucciones por Row */

/* Prototipo de funciones */
/* -------------------------------------------------------------------------- */
/* Funci�n para busqueda de Baud Rate */
struct datos_archivo_baud_txt BusqBaudRate( void );

#endif	/* AUTOBAUDRATE_H */

