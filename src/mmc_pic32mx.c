/*------------------------------------------------------------------------/
/  MMCv3/SDv1/SDv2 (in SPI mode) control module
/-------------------------------------------------------------------------/
/
/  Copyright (C) 2014, ChaN, all right reserved.
/
/ * This software is a free software and there is NO WARRANTY.
/ * No restriction on use. You can use, modify and redistribute it for
/   personal, non-profit or commercial products UNDER YOUR RESPONSIBILITY.
/ * Redistributions of source code must retain the above copyright notice.
/
/-------------------------------------------------------------------------*/

#include <spi.h>
#include "diskio.h"

#include "dma.h"
#include "ArtLogSD.h"
#include <plib.h>
//#include <xc.h>

/* Socket controls  (Platform dependent) */
//#define CS_LOW()  _LATE0 = 0	/* MMC CS = L */
//#define CS_HIGH() _LATE0 = 1	/* MMC CS = H */
#define CS_SETOUT()     TRISBbits.TRISB11 = 0 
#define CS_LOW()        LATBbits.LATB11 = 0      //MMC CS = L
#define CS_HIGH()       LATBbits.LATB11 = 1      //MMC CS = H

//Change the SPI port number as needed on the following 5 lines
#define SPIBRG          SPI1BRG
#define SPIBUF          SPI1BUF
#define SPISTATbits     SPI1STATbits
#define SPI_CHANNEL     SPI_CHANNEL1
#define SPICONbits      SPI1CONbits

/* Cambios para DMA */
#define mmc_pic32mxUSE_DMA      0

#if ( mmc_pic32mxUSE_DMA == 1 )    
    #define _SPI_TX_IRQ     _SPI1_TX_IRQ
    #define SPISTATCLR      SPI1STATCLR
    #define DMA_CHANNEL     DMA_CHANNEL1
    #define _DMA_VECTOR     _DMA1_VECTOR
#endif

//#define CD	(!_RB11)	/* Card detected   (yes:true, no:false, default:true) */
//#define WP	(_RB10)		/* Write protected (yes:true, no:false, default:false) */
#define CD              0       /* Card detected   (yes:true, no:false, default:true) */
#define WP              0       /* Write protected (yes:true, no:false, default:false) */

///* SPI bit rate controls */
#define	FCLK_SLOW()	SPIBRG = 64		/* Set slow clock (100k-400k) */
#define	FCLK_FAST()	SPIBRG = 2		/* Set fast clock (depends on the CSD) */

/*--------------------------------------------------------------------------

   Module Private Functions

---------------------------------------------------------------------------*/

/* Definitions for MMC/SDC command */
#define CMD0   (0)			/* GO_IDLE_STATE */
#define CMD1   (1)			/* SEND_OP_COND */
#define ACMD41 (41|0x80)	/* SEND_OP_COND (SDC) */
#define CMD8   (8)			/* SEND_IF_COND */
#define CMD9   (9)			/* SEND_CSD */
#define CMD10  (10)			/* SEND_CID */
#define CMD12  (12)			/* STOP_TRANSMISSION */
#define ACMD13 (13|0x80)	/* SD_STATUS (SDC) */
#define CMD16  (16)			/* SET_BLOCKLEN */
#define CMD17  (17)			/* READ_SINGLE_BLOCK */
#define CMD18  (18)			/* READ_MULTIPLE_BLOCK */
#define CMD23  (23)			/* SET_BLOCK_COUNT */
#define ACMD23 (23|0x80)	/* SET_WR_BLK_ERASE_COUNT (SDC) */
#define CMD24  (24)			/* WRITE_BLOCK */
#define CMD25  (25)			/* WRITE_MULTIPLE_BLOCK */
#define CMD41  (41)			/* SEND_OP_COND (ACMD) */
#define CMD55  (55)			/* APP_CMD */
#define CMD58  (58)			/* READ_OCR */


static volatile
DSTATUS Stat = STA_NOINIT;	/* Disk status */

static volatile
UINT Timer1, Timer2;		/* 1000Hz decrement timer */

static
UINT CardType;

#if ( mmc_pic32mxUSE_DMA == 1 )
    /* Variables DMA */
    volatile int    DmaTxIntFlag;			/* flag used in interrupts, signal that DMA transfer ended */
    volatile int	DmaRxIntFlag;			/* flag used in interrupts, signal that DMA transfer ended */
    volatile int 	DMAINTCOUNT = 0;

    /*-----------------------------------------------------------------------*/
    /* Start a DMA Sector Transfer between host to card     */
    /*-----------------------------------------------------------------------*/
    BYTE sectorsendDMA( const BYTE *buff );
#endif    
/*-----------------------------------------------------------------------*/
/* Transmit/Receive data to/from MMC via SPI  (Platform dependent)       */
/*-----------------------------------------------------------------------*/

/* Single byte SPI transfer */

static 
BYTE xchg_spi (BYTE dat)
{   
    SPIBUF = dat;                       /* Buffer para datos de escritura y lectura */
	while (!SPISTATbits.SPIRBF) ;
	return (BYTE)SPIBUF;   
}

/*-----------------------------------------------------------------------*/
/* Wait for card ready                                                   */
/*-----------------------------------------------------------------------*/

static
int wait_ready (void)
{
	BYTE d;

	Timer2 = 500;           /* Wait for ready in timeout of 500ms */
    
    //if( xTimerOut != NULL ){ xTimerStart( xTimerOut, 0 ); }
    
	do
		d = xchg_spi(0xFF);
	while ((d != 0xFF) && Timer2);
    
    //xTimerStop( xTimerOut, 0 );
    //printf( "%d\n",d);

	return (d == 0xFF) ? 1 : 0;
}

/*-----------------------------------------------------------------------*/
/* Deselect the card and release SPI bus                                 */
/*-----------------------------------------------------------------------*/

static
void deselect (void)
{  
	CS_HIGH();			/* Set CS# high */
	xchg_spi(0xFF);		/* Dummy clock (force MMC DO hi-z for multiple slave SPI) */
}

/*-----------------------------------------------------------------------*/
/* Select the card and wait ready                                        */
/*-----------------------------------------------------------------------*/

static
int select (void)                   /* 1:Successful, 0:Timeout */
{
	CS_LOW();                       /* Set CS# low */
	xchg_spi(0xFF);                 /* Dummy clock (force MMC DO enabled) */

	if (wait_ready()) 
    {
        //printf( "   OK...[select]\n" );
        return 1;     /* Wait for card ready */
    }
        //printf( "Fallo...[select]\n" );
	deselect();
	return 0;                       /* Timeout */
}

/*-----------------------------------------------------------------------*/
/* Power Control  (Platform dependent)                                   */
/*-----------------------------------------------------------------------*/
/* When the target system does not support socket power control, there   */
/* is nothing to do in these functions.                                  */

static
void power_on (void)
{
    //;                       /* Turn on socket power, delay >1ms (Nothing to do) */

  //SPI1CON = 0x0000013B;	/* Enable SPI1 */    
  //SPI1CON = 0x000010B7;	/* Enable SPI1 */
  //SPI1CON = 0x00000120;	/* Enable SPI1 */
                            /* Bit 31   : FRMEN = 0   : Framed SPI support is disabled           */                            
                            /* Bit 30   : FRMSYNC = 0 : only Framed SPI mode                     */
                            /* Bit 29   : FRMPOL = 0  : only Framed SPI mode                     */
                            /* Bit 28   : MSSEN = 0   : Slave select SPI support is disabled     */    
                            /* Bit 27   : FRMSYPW = 0 : Frame sync pulse is one clock wide       */    
                            /* Bit 26-24: FRMSYPW =0x0: Generate a frame sync pulse on every data character       */    
                            /* Bit 23-18:             : Read as ?0?                               */
                            /* Bit 17   : SPIFE = 0   : only Framed SPI mode                     */
                            /* Bit 16   : ENHBUF = 0  : Enhanced Buffer mode is disabled         */
                            /* Bit 15   : ON = 0      : SPI Peripheral is disabled               */
                            /* Bit 14   :             : Read as ?0?                               */
                            /* Bit 13   : SIDL = 1    : Discontinue operation when CPU enters in Idle mode        */
                            /* Bit 12   : DISSDO = 0  : SDO1 pin is controlled by the module                      */
                            /* Bit 11-10: MODE = 00   : Communication 8-bit                      */
                            /* Bit 9    : SMP = 0     : Master mode (MSTEN = 1): 0 = Input data sampled at middle of data output time       */
                            /* Bit 8    : CKE = 1     : Serial output data changes on transition from active clock state to Idle clock state (see CKP bit) */
                            /* Bit 7    : SSEN = 0    : SS1 pin not used for Slave mode (pin is controlled by port function)                */
                            /* Bit 6    : CKP = 1     : Idle state for clock is a high level; active state is a low level                   */  
                            /* Bit 5    : MSTEN = 1   : Master mode                              */  
                            /* Bit 4    :             : Read as ?0?                               */
                            /* Bit 3-2  : STXISE = 01 : Interrupt is generated when the buffer is completely empty                          */
                            /* Bit 1-0  : SRXISEL = 11: Interrupt is generated when the buffer is full            */
    
    
    //SPI1CONbits.ON = 1;
    
   //SPI1BRG = 120;           /* Clock divider 256 Kbps @ PBCLK 40 MHz (77) */
    
                                /* Setup SPI2 */
                                /* Setup SPI */
	// Setup CS as output
	//CS_SETOUT();
	// configured for ~400 kHz operation - reset later to 20 MHz
//	SpiChnOpen(SPI_CHANNEL2,SPI_OPEN_MSTEN|SPI_OPEN_CKP_HIGH|SPI_OPEN_SMP_END|SPI_OPEN_MODE8,64); 
	SpiChnOpen(SPI_CHANNEL,SPI_OPEN_MSTEN|SPI_OPEN_CKP_HIGH|SPI_OPEN_SMP_END|SPI_OPEN_MODE8,64); 
//	SPI2CONbits.ON = 1;
	SPICONbits.ON = 1;  
    
}

static
void power_off (void)
{
	//;                         /* Turn off port and socket power (Nothing to do) */
    
    select();                   /* Wait for card ready */
	deselect();
    
	SPICONbits.ON = 0;			/* Disable SPI */

	Stat |= STA_NOINIT;         /* Set STA_NOINIT */
}

/*-----------------------------------------------------------------------*/
/* Transmit/Receive data to/from MMC via SPI  (Platform dependent)       */
/*-----------------------------------------------------------------------*/

/* Block SPI transfers */

static
void xmit_spi_multi (
	const BYTE* buff,	/* Data to be sent */
	UINT cnt			/* Number of bytes to send */
)
{
	do {
		SPIBUF = *buff++; while (!SPISTATbits.SPIRBF) ; SPIBUF;
		SPIBUF = *buff++; while (!SPISTATbits.SPIRBF) ; SPIBUF;
	} while (cnt -= 2);
}


static
void rcvr_spi_multi (
	BYTE* buff,         /* Buffer to store received data */
	UINT cnt            /* Number of bytes to receive */
)
{
	do {
		SPIBUF = 0xFF; while (!SPISTATbits.SPIRBF) ; *buff++ = SPIBUF;
		SPIBUF = 0xFF; while (!SPISTATbits.SPIRBF) ; *buff++ = SPIBUF;
	} while (cnt -= 2);
}

/*-----------------------------------------------------------------------*/
/* Receive a data packet from MMC                                        */
/*-----------------------------------------------------------------------*/

static
int rcvr_datablock (                /* 1:OK, 0:Failed */
	BYTE *buff,                     /* Data buffer to store received data */
	UINT btr                        /* Byte count (must be multiple of 4) */
)
{
	BYTE token;


	Timer1 = 100;
	do {							/* Wait for data packet in timeout of 100ms */
		token = xchg_spi(0xFF);
        //printf("Timer1: %i\n",Timer1);
	} while ((token == 0xFF) && Timer1);

	if(token != 0xFE) return 0;		/* If not valid data token, retutn with error */
  
	rcvr_spi_multi(buff, btr);		/* Receive the data block into buffer */
	xchg_spi(0xFF);					/* Discard CRC */
	xchg_spi(0xFF);

	return 1;						/* Return with success */
}



/*-----------------------------------------------------------------------*/
/* Send a data packet to MMC                                             */
/*-----------------------------------------------------------------------*/

#if _USE_WRITE
static
int xmit_datablock (                /* 1:OK, 0:Failed */
	const BYTE *buff,               /* 512 byte data block to be transmitted */
	BYTE token                      /* Data token */
)
{
	BYTE resp;


	if (!wait_ready()) 
    {
        printf( "Fallo...[xmit_datablock]\n" );
        return 0;
    }
        

	xchg_spi(token);                /* Xmit a token */
	if (token != 0xFD) {            /* Not StopTran token */
        
		//xmit_spi_multi(buff, 512);	/* Xmit the data block to the MMC */
        #if ( mmc_pic32mxUSE_DMA == 1 )
            sectorsendDMA(buff);        /* Envio de datos por DMA */
        #else    
            xmit_spi_multi(buff, 512);	/* Xmit the data block to the MMC */
        #endif
            
		xchg_spi(0xFF);				/* CRC (Dummy) */
		xchg_spi(0xFF);
		resp = xchg_spi(0xFF);		/* Receive a data response */
		if ((resp & 0x1F) != 0x05)	/* If not accepted, return with error */
			return 0;
	}

	return 1;
}
#endif



/*-----------------------------------------------------------------------*/
/* Send a command packet to MMC                                          */
/*-----------------------------------------------------------------------*/

static
BYTE send_cmd (
	BYTE cmd,		/* Command byte */
	DWORD arg		/* Argument */
)
{
	BYTE n, res;


	if (cmd & 0x80) {	/* ACMD<n> is the command sequense of CMD55-CMD<n> */
		cmd &= 0x7F;
		res = send_cmd(CMD55, 0);
		if (res > 1) return res;
	}

	/* Select the card and wait for ready except to stop multiple block read */
	if (cmd != CMD12) {
		deselect();       
		if (!select()) 
        {
            printf("Fallo...[send_cmd]\n");
            return 0xFF;
        }
        
            
	}

	/* Send command packet */
	xchg_spi(0x40 | cmd);			/* Start + Command index */
	xchg_spi((BYTE)(arg >> 24));	/* Argument[31..24] */
	xchg_spi((BYTE)(arg >> 16));	/* Argument[23..16] */
	xchg_spi((BYTE)(arg >> 8));		/* Argument[15..8] */
	xchg_spi((BYTE)arg);			/* Argument[7..0] */
	n = 0x01;						/* Dummy CRC + Stop */
	if (cmd == CMD0) n = 0x95;		/* Valid CRC for CMD0(0) + Stop */
	if (cmd == CMD8) n = 0x87;		/* Valid CRC for CMD8(0x1AA) + Stop */
	xchg_spi(n);

	/* Receive command response */
	if (cmd == CMD12) xchg_spi(0xFF);	/* Skip a stuff byte on stop to read */
	n = 10;							/* Wait for a valid response in timeout of 10 attempts */
	do
		res = xchg_spi(0xFF);
	while ((res & 0x80) && --n);

	return res;			/* Return with the response value */
}

/*--------------------------------------------------------------------------

   Public Functions

---------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------*/
/* Get Disk Status                                                       */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status (
	BYTE pdrv		/* Physical drive nmuber (0) */
)
{
	if (pdrv != 0) return STA_NOINIT;	/* Supports only single drive */

	return Stat;
}

/*-----------------------------------------------------------------------*/
/* Initialize Disk Drive                                                 */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize (
	BYTE pdrv		/* Physical drive nmuber (0) */
)
{
	BYTE n, cmd, ty, ocr[4];


	if (pdrv != 0) return STA_NOINIT;       /* Supports only single drive */
	if (Stat & STA_NODISK) return Stat;     /* No card in the socket */

	power_on();                             /* Initialize memory card interface */
	FCLK_SLOW();
	for (n = 10; n; n--) xchg_spi(0xFF);	/* 80 dummy clocks */
 
	ty = 0;
	if (send_cmd(CMD0, 0) == 1) {			/* Enter Idle state */
		Timer1 = 1000;						/* Initialization timeout of 1000 msec */
		if (send_cmd(CMD8, 0x1AA) == 1) {	/* SDv2? */
			for (n = 0; n < 4; n++) ocr[n] = xchg_spi(0xFF);			/* Get trailing return value of R7 resp */
			if (ocr[2] == 0x01 && ocr[3] == 0xAA) {                     /* The card can work at vdd range of 2.7-3.6V */
				while (Timer1 && send_cmd(ACMD41, 0x40000000));         /* Wait for leaving idle state (ACMD41 with HCS bit) */
				if (Timer1 && send_cmd(CMD58, 0) == 0) {                /* Check CCS bit in the OCR */
					for (n = 0; n < 4; n++) ocr[n] = xchg_spi(0xFF);
					ty = (ocr[0] & 0x40) ? CT_SD2|CT_BLOCK : CT_SD2;	/* SDv2 */
				}
			}
		} else {                                        /* SDv1 or MMCv3 */
			if (send_cmd(ACMD41, 0) <= 1) 	{
				ty = CT_SD1; cmd = ACMD41;              /* SDv1 */
			} else {
				ty = CT_MMC; cmd = CMD1;                /* MMCv3 */
			}
			while (Timer1 && send_cmd(cmd, 0));         /* Wait for leaving idle state */
			if (!Timer1 || send_cmd(CMD16, 512) != 0)	/* Set read/write block length to 512 */
				ty = 0;
		}
	}
    
	CardType = ty;
	deselect();

	if (ty) {                   /* Function succeded */
		Stat &= ~STA_NOINIT;	/* Clear STA_NOINIT */
		FCLK_FAST();
        //printf("Stat: %d\n", Stat);
	} else {                    /* Function failed */
		power_off();            /* Deinitialize interface */
	}

	return Stat;
}

/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read (
	BYTE pdrv,		/* Physical drive nmuber (0) */
	BYTE *buff,		/* Pointer to the data buffer to store read data */
	DWORD sector,	/* Start sector number (LBA) */
	UINT count		/* Sector count (1..128) */
)
{    
	if (pdrv || !count) return RES_PARERR;
	if (Stat & STA_NOINIT) return RES_NOTRDY;

	if (!(CardType & CT_BLOCK)) sector *= 512;      /* Convert to byte address if needed */

	if (count == 1) {                               /* Single block read */
        
		if ((send_cmd(CMD17, sector) == 0)          /* READ_SINGLE_BLOCK */
			&& rcvr_datablock(buff, 512))
			count = 0;
        //printf("res_send_cmd: %i\n",res_send_cmd);
        //printf("res_rcvr_datablock: %i\n",res_rcvr_datablock);
	}
	else {                                          /* Multiple block read */
		if (send_cmd(CMD18, sector) == 0) {         /* READ_MULTIPLE_BLOCK */
			do {
				if (!rcvr_datablock(buff, 512)) break;
				buff += 512;
			} while (--count);
			send_cmd(CMD12, 0);                     /* STOP_TRANSMISSION */
		}
	}
	deselect();

	return count ? RES_ERROR : RES_OK;
}

/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if _USE_WRITE
DRESULT disk_write (
	BYTE pdrv,				/* Physical drive nmuber (0) */
	const BYTE *buff,		/* Pointer to the data to be written */
	DWORD sector,			/* Start sector number (LBA) */
	UINT count				/* Sector count (1..128) */
)
{
	if (pdrv || !count) return RES_PARERR;
	if (Stat & STA_NOINIT) return RES_NOTRDY;
	if (Stat & STA_PROTECT) return RES_WRPRT;

	if (!(CardType & CT_BLOCK)) sector *= 512;	/* Convert to byte address if needed */

	if (count == 1) {                           /* Single block write */
		if ((send_cmd(CMD24, sector) == 0)      /* WRITE_BLOCK */
			&& xmit_datablock(buff, 0xFE))
			count = 0;
	}
	else {                                      /* Multiple block write */
		if (CardType & CT_SDC) send_cmd(ACMD23, count);
		if (send_cmd(CMD25, sector) == 0) {     /* WRITE_MULTIPLE_BLOCK */
			do {
				if (!xmit_datablock(buff, 0xFC)) break;
				buff += 512;
			} while (--count);
			if (!xmit_datablock(0, 0xFD))       /* STOP_TRAN token */
				count = 1;
		}
	}
	deselect();

	return count ? RES_ERROR : RES_OK;
}
#endif

/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

#if _USE_IOCTL
DRESULT disk_ioctl (
	BYTE pdrv,		/* Physical drive nmuber (0) */
	BYTE cmd,		/* Control code */
	void *buff		/* Buffer to send/receive data block */
)
{
	DRESULT res;
	BYTE n, csd[16], *ptr = buff;
	DWORD csz;


	if (pdrv) return RES_PARERR;
	if (Stat & STA_NOINIT) return RES_NOTRDY;

	res = RES_ERROR;
	switch (cmd) {
	case CTRL_SYNC :	/* Flush write-back cache, Wait for end of internal process */
		if (select()) res = RES_OK;
		break;

	case GET_SECTOR_COUNT :	/* Get number of sectors on the disk (WORD) */
		if ((send_cmd(CMD9, 0) == 0) && rcvr_datablock(csd, 16)) {
			if ((csd[0] >> 6) == 1) {	/* SDv2? */
				csz = csd[9] + ((WORD)csd[8] << 8) + ((DWORD)(csd[7] & 63) << 16) + 1;
				*(DWORD*)buff = csz << 10;
			} else {					/* SDv1 or MMCv3 */
				n = (csd[5] & 15) + ((csd[10] & 128) >> 7) + ((csd[9] & 3) << 1) + 2;
				csz = (csd[8] >> 6) + ((WORD)csd[7] << 2) + ((WORD)(csd[6] & 3) << 10) + 1;
				*(DWORD*)buff = csz << (n - 9);
			}
			res = RES_OK;
		}
		break;

	case GET_BLOCK_SIZE :	/* Get erase block size in unit of sectors (DWORD) */
		if (CardType & CT_SD2) {	/* SDv2? */
			if (send_cmd(ACMD13, 0) == 0) {		/* Read SD status */
				xchg_spi(0xFF);
				if (rcvr_datablock(csd, 16)) {				/* Read partial block */
					for (n = 64 - 16; n; n--) xchg_spi(0xFF);	/* Purge trailing data */
					*(DWORD*)buff = 16UL << (csd[10] >> 4);
					res = RES_OK;
				}
			}
		} else {					/* SDv1 or MMCv3 */
			if ((send_cmd(CMD9, 0) == 0) && rcvr_datablock(csd, 16)) {	/* Read CSD */
				if (CardType & CT_SD1) {	/* SDv1 */
					*(DWORD*)buff = (((csd[10] & 63) << 1) + ((WORD)(csd[11] & 128) >> 7) + 1) << ((csd[13] >> 6) - 1);
				} else {					/* MMCv3 */
					*(DWORD*)buff = ((WORD)((csd[10] & 124) >> 2) + 1) * (((csd[11] & 3) << 3) + ((csd[11] & 224) >> 5) + 1);
				}
				res = RES_OK;
			}
		}
		break;

	case MMC_GET_TYPE :		/* Get card type flags (1 byte) */
		*ptr = CardType;
		res = RES_OK;
		break;

	case MMC_GET_CSD :	/* Receive CSD as a data block (16 bytes) */
		if ((send_cmd(CMD9, 0) == 0)	/* READ_CSD */
			&& rcvr_datablock(buff, 16))
			res = RES_OK;
		break;

	case MMC_GET_CID :	/* Receive CID as a data block (16 bytes) */
		if ((send_cmd(CMD10, 0) == 0)	/* READ_CID */
			&& rcvr_datablock(buff, 16))
			res = RES_OK;
		break;

	case MMC_GET_OCR :	/* Receive OCR as an R3 resp (4 bytes) */
		if (send_cmd(CMD58, 0) == 0) {	/* READ_OCR */
			for (n = 0; n < 4; n++)
				*((BYTE*)buff+n) = xchg_spi(0xFF);
			res = RES_OK;
		}
		break;

	case MMC_GET_SDSTAT :	/* Receive SD statsu as a data block (64 bytes) */
		if ((CardType & CT_SD2) && send_cmd(ACMD13, 0) == 0) {	/* SD_STATUS */
			xchg_spi(0xFF);
			if (rcvr_datablock(buff, 64))
				res = RES_OK;
		}
		break;

	case CTRL_POWER_OFF :	/* Power off */
		power_off();
		Stat |= STA_NOINIT;
		res = RES_OK;
		break;

	default:
		res = RES_PARERR;
	}

	deselect();

	return res;
}
#endif

/*-----------------------------------------------------------------------*/
/* Device Timer Driven Procedure                                         */
/*-----------------------------------------------------------------------*/
/* This function must be called by timer interrupt in period of 1ms      */

void disk_timerproc (void)
{
	BYTE s;
	UINT n;


	n = Timer1;					/* 1000Hz decrement timer with zero stopped */
	if (n) Timer1 = --n;
	n = Timer2;
	if (n) Timer2 = --n;


	/* Update socket status */

	s = Stat;

	if (WP) s |= STA_PROTECT;
	else	s &= ~STA_PROTECT;

	if (CD) s &= ~STA_NODISK;
	else	s |= (STA_NODISK | STA_NOINIT);

	Stat = s;
}

#if ( mmc_pic32mxUSE_DMA == 1 )
/*-----------------------------------------------------------------------*/
/* Start a DMA Sector Transfer between host to card     */
/*-----------------------------------------------------------------------*/
BYTE sectorsendDMA(const BYTE *buff)
{
	// int evFlags;
	int done=0; 
    
	/* Clear SPI TX empty int */
	INTClearFlag(INT_SOURCE_SPI(_SPI_TX_IRQ));
    
    /* Tx */
	DmaChnOpen(DMA_CHANNEL, DMA_CHN_PRI3, DMA_OPEN_DEFAULT);     
	DmaChnSetEventControl(DMA_CHANNEL, DMA_EV_START_IRQ_EN|DMA_EV_START_IRQ(_SPI_TX_IRQ));
	DmaChnSetTxfer(DMA_CHANNEL,(unsigned char*)buff, (void*)&SPIBUF, 512, 1, 1);
	DmaChnSetEvEnableFlags(DMA_CHANNEL, DMA_EV_BLOCK_DONE);	

	INTSetVectorPriority(INT_VECTOR_DMA(DMA_CHANNEL), INT_PRIORITY_LEVEL_5);        /* Set INT controller priority */		
	INTSetVectorSubPriority(INT_VECTOR_DMA(DMA_CHANNEL), INT_SUB_PRIORITY_LEVEL_2); /* Set INT controller sub-priority */

	INTEnable(INT_SOURCE_DMA(DMA_CHANNEL), INT_ENABLED);		/* Enable the chn interrupt in the INT controller */
	DmaTxIntFlag=0;                                             /* clear the interrupt flag it's waiting on */
	//clear SPI TX empty int
	//DmaChnEnable(DMA_CHANNEL1);
	DmaChnStartTxfer(DMA_CHANNEL, DMA_WAIT_NOT, 0); 
	
	while(!DmaTxIntFlag) {}
	//DmaChnDisable(DMA_CHANNEL1);
	/* Disable SPI interrupt */
	INTEnable(INT_SOURCE_DMA(DMA_CHANNEL), INT_DISABLED);
	//while(SPI4STATbits.SPIBUSY==1){}
    while(SPISTATbits.SPIBUSY==1){}

	done=SPIBUF;
	return(done);
}

/* handler for the DMA channel 1 interrupt */
void __ISR(_DMA_VECTOR, IPL5_AUTO) DmaHandler(void)
{
	int	evFlags;				// event flags when getting the interrupt
	SPISTATCLR=0x40;
	volatile unsigned char discard=SPIBUF;
	discard++;
	INTClearFlag(INT_SOURCE_DMA(DMA_CHANNEL));	// acknowledge the INT controller, we're servicing int
	evFlags=DmaChnGetEvFlags(DMA_CHANNEL);	// get the event flags
    if(evFlags&DMA_EV_BLOCK_DONE)
    { // just a sanity check. we enabled just the DMA_EV_BLOCK_DONE transfer done interrupt
    	DmaTxIntFlag=1;
 		DmaChnClrEvFlags(DMA_CHANNEL, DMA_EV_BLOCK_DONE);
    }
	DMAINTCOUNT++;
}
#endif

void TimeOut( void *pvParameters )
{
    /* Evita warnings del compilador */
    (void) pvParameters;
    
    UINT n;

	n = Timer1;					
	if (n) Timer1 = --n;
	n = Timer2;
	if (n) Timer2 = --n;
}