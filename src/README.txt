Equipo ArtLogSD by OIGP

Versión 1.3     Se incluye funcionalidad de lectura de archivo baud.txt para configuración
                Se permite cambio en modo de operación de ArtLogSD, Inversión o 
                No Inversión de datos recibidos por RX, guardar dato de Baud rate

                Etiquetas permitidas en buad.txt
                Primer parámetro: antes de la coma indica baudaje o AUTO ( Modo Autobaudaje )
                Segundo parámetro: despues de la coma Inversión o no Inversión de los datos recibidos en RX

                Ejemplos:
                AUTO,0              **Nota: Modo Auto Baud Rate sin Inversión de datos recibidos por RX
                auto,1              **Nota: Modo Auto Baud Rate con Inversión de datos recibidos por RX    
                115200,false        **Nota: Modo Normal a 115200 Rate sin Inversión de datos recibidos por RX
                500000,TRUE         **Nota: Modo Normal a 500000 Rate con Inversión de datos recibidos por RX

                Los datos se indican en una sola línea.
                
Versión 1.2     Se incluye funcionalidad de Run-Time Behavior (Solo uso para programador)
                Se incluye funcionalidad de Auto BaudRate  

Versión 1.1     Se agregan Mensajes de Vin a archivos LGSDxxxx.txt
                Se cambia rutina de LEDs

Versión 1.0     Primera versión liberada