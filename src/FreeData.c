/* Archivo:         FreeData.c
 * Autor:           Olaf I. G�mez P.
 * Fecha:           16/Diciembre/15
 * uC:              PIC32MX170F256B
 * Versi�n:         0
 * Descripci�n:     Archivo para Task "FreeData"
 *                    
 */

/* App Include */
#include "memoryCard.h"

/* -------------------------------------------------------------------------- */
/* Nombre de la Tarea:  FreeData
 * Objetivo:            Libera la informaci�n de la interrupci�n que no se ha 
 *                      guardado
 * Observaciones:        */
void FreeData ( void *pvParameters )
{
    /* Solo para no tener warnings */
    ( void ) pvParameters;
    
    /* Definici�n de Variables  */
    uint8_t dataStatusLED;
    uint8_t *dataTask;
    
    /* Inicializaci�n de Variables */
    
    /* Definici�n de variables */
    FRESULT FOpenRes;
    uint32_t numread;
   
    for( ;; )
    {
                
        /* �Hay datos por liberar */
        xQueueReceive( xRxedCharsAux, &dataTask, portMAX_DELAY );  
        
            /* Se almacena la informaci�n */
            FOpenRes = f_write(&file1, dataTask, strlen(dataTask), &numread);
            if( numread == 0 )      /* Indica que dataTask esta vacia */
            {
                printf( "[  FreeData   ] No hay datos por guardar\n" );
            }
            else
            {
                printf( "[  FreeData   ] Banco Ex: %u\n", numread );   
            }
            f_sync( &file1 );
            
            /* Se libera memoria din�mica */
            vPortFree(dataTask);        /* Se libera memoria*/
            
            /* Se coloca dato para env�o de estado de LEDs */
            dataStatusLED = 0xC0;
            xQueueSendToBack(xStatusLED, &dataStatusLED, NOT_WAIT_TIME);
            
            /* Limpiar RX_LED */
            LED1_OFF();      
    }
}