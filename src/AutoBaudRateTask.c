/* Archivo:         AutoBaudRateTask.c
 * Autor:           Olaf I. G�mez P.
 * Fecha:           15/Febrero/16
 * uC:              PIC32MX170F256B
 * Versi�n:         0
 * Descripci�n:     Archivo para Task "AutoBaudRate"
 *                    
 */

/* Include */
#include "ArtLogSD.h"
#include "memoryCard.h"
#include "AutoBaudRate.h"
//#include "serial.h"

/* Variables para Timers  */
static volatile uint32_t timerCD_SW_2;
uint32_t timerBaudRate;

struct datos_archivo_baud_txt{    
    uint8_t     modo_de_operacion_ArtLogSD;
    uint8_t     modo_de_baud_rate;
    uint32_t    baud_rate_archivo; 
}datos_txt;

/* -------------------------------------------------------------------------- */
/* Nombre de la Tarea:  ABaudRTask
 * Objetivo:            Verificar si hay condiciones para entrar en modo Auto 
 *                      Baud Rate */
void ABaudRTask ( void *pvParameters )
{
    /* Solo para no tener warnings */
    ( void ) pvParameters;
    
    /* Declaraciones */
    uint8_t auxTime;                           /* Variable para contar tiempo */
    uint8_t dataStatusLED;   
    uint32_t baudRate_NVM;
    struct datos_archivo_baud_txt datos_a_evaluar;
        
    /* Inicializaci�n de variables */
    
    for( ;; )
    {   
        /* Tasks Suspend */
        vTaskSuspend( pxInitSDTask );
        vTaskSuspend( pxWriteSDTask );
        vTaskSuspend( pxCountFData );
        vTaskSuspend( pxFreeData );
        //vTaskSuspend( pxLedTask );
        vTaskSuspend( pxADCTask );
        
        //vTaskDelay( 10 / portTICK_PERIOD_MS );
        
        /* Aviso para introducir la memoria */        
        if( CD_SW )
        {
            delay_ms( 200 );
            if( CD_SW )
            {
                printf( "Antes de continuar coloque la memoria microSD ...\n" );   
            }
            delay_ms( 2000 );
        }
        else
        {
            printf( "Memoria microSD Detectada ...\n" );
        
            /* Se realiza la busqueda de archivo baud.txt */
            datos_a_evaluar = BusqBaudRate();

            printf( "Modo de operaci�n ArtLogSD : %u \n", datos_a_evaluar.modo_de_operacion_ArtLogSD  );
            printf( "Baud Rate Archivo          : %u \n", datos_a_evaluar.baud_rate_archivo  );
            printf( "Modo Baud Rate             : %u \n", datos_a_evaluar.modo_de_baud_rate );

            /* Se verifica si ya se tiene Baud Rate para arrancar */
            //if( MAGIC_WORD == *( uint32_t * )( NVM_PROGRAM_PAGE ) )
            if ( 0 == datos_a_evaluar.modo_de_operacion_ArtLogSD )
            {
                /* Se lee el baudaje de la NVM */
                //baudRate_NVM = *( uint32_t * )( NVM_PROGRAM_PAGE + 0x0004 );
                baudRate_NVM = datos_a_evaluar.baud_rate_archivo;

                /* Mensaje a 500000bps */
                printf( "El equipo ArtLogSD tiene un Baud Rate configurado.....\n" );
                printf( "El Baud Rate para Tx/Rx se ajustara a %ubps...\n", baudRate_NVM );

                /* Configuraci�n de Inversi�n de RX */
                if ( 1 == datos_a_evaluar.modo_de_baud_rate ){
                    UARTConfigure( UARTx, UART_ENABLE_PINS_TX_RX_ONLY | UART_ENABLE_HIGH_SPEED | UART_INVERT_RECEIVE_POLARITY );
                } 

                /* Se cambia el baudaje al detectado */
                UARTSetDataRate( UARTx, PERIPHERAL_CLOCK, baudRate_NVM );

                /* Tiempo para estabilizaci�n de configuraci�n */
                delay_ms(200);      /* 0.2 seg */

                /* Mensaje de Entrada */
//                printf( "ARTLOGSD V%1.1f \n", FWVersion );

                /* Env�o de mensaje para verificar si se tiene la petici�n */
                printf("Auto Baud Rate ");

                if( CD_SW )    /* NO hay presencia de Memoria SD al arrancar ArtLogSD */
                { 
                    /* Env�o de dato para estado de LEDs */
                    dataStatusLED = 0x91;
                    xQueueSendToBack(xStatusLED, &dataStatusLED, NOT_WAIT_TIME);

                    /* Se inicializa contador para eliminar rebotes de Switch */
                    timerCD_SW_2 = 500;           /* 0.5 seg */
                    do{Nop();}
                    while( timerCD_SW_2 );

                    /* SD Card Presente  */
                    if( CD_SW )
                    {
                        printf("Conteo... \n");

                        for( auxTime = 11; auxTime >= 2 ; auxTime-- )
                        {
                            printf("Tiempo restante para entrar a Modo Auto Baud Rate: %u seg. \n", auxTime - 1 );

                            timerCD_SW_2 = 1000;  /* 1 seg */
                            do{Nop();}
                            while( timerCD_SW_2 );

                            if( !CD_SW )
                            {
                                timerCD_SW_2 = 500;           /* 0.5 seg */
                                do{Nop();}
                                while( timerCD_SW_2 );

                                /* Entra a Modo Auto Baud Rate */
                                if( !CD_SW )
                                {
                                    /* Env�o de mensaje para verificar si se tiene la petici�n */
                                    printf("Auto Baud Rate Activado... \n");

                                    /* Se cambia la configuraci�n del pin PRB6 modo Input Capture */
                                    /* NOTA IMPORTANTE: Proceso cr�tico, se suspenden todas las tareas y
                                     * todas las interrupciones debido a cambio de configuraci�n de PIN */

                                    /* Tasks Suspend */
                                    /* Las tareas se inicializan en modo Suspedidas */

                                    /* Timers Stop */
                                    xTimerStop( xTimerOutFreeD, 0 ); 
                                    xTimerStop( xTimerOutCD_SW_1, 0 ); 
                                    xTimerStop( xTimerOutCD_SW_2, 0 ); 
                                    xTimerStop( xTimerOutLED, 0 );
                                    xTimerStop( xTimerOutVin, 0 );

                                    /* Deshabilitaci�n de Interrupciones */
                                    INTEnable( INT_SOURCE_UART_RX( UARTx ), INT_DISABLED );
                                    INTEnable( INT_IC1, INT_DISABLED );

                                        /* INICIO Programacion de PPS */
                                        PPSUnLock;
                                        /* Input Capture 1 */   
                                            PPSInput( 3, IC1, RPB6 );
                                        /* FIN Programacion de PPS */
                                        PPSLock;

                                        /* Estabilizaci�n por configuraci�n */
                                        delay_ms(500);      /* 0.5 seg */

                                    /* Habilitaci�n de Interrupciones */  
                                    INTEnable( INT_IC1, INT_ENABLED );

                                    /* Timers Start */
                                    xTimerStart( xTimerOutFreeD, 0 ); 
                                    xTimerStart( xTimerOutCD_SW_1, 0 ); 
                                    xTimerStart( xTimerOutCD_SW_2, 0 ); 
                                    xTimerStart( xTimerOutLED, 0 );
                                    xTimerStart( xTimerOutVin, 0 );

                                    /* Entra a Modo Auto Baud Rate*/
                                    vMedBitTime();                        

                                    /* Terminar for */
                                    auxTime = 1;
                                }
                                else
                                {
                                    /* Env�o de mensaje para verificar si se tiene la petici�n */
                                    printf("Auto Baud Rate NO Activado... \n");

                                    /* Env�o de dato para estado de LEDs */
                                    dataStatusLED = 0x90;
                                    xQueueSendToBack( xStatusLED, &dataStatusLED, NOT_WAIT_TIME );

                                    /* Se Arrancan las Tareas */
                                    vTaskResume( pxInitSDTask );
                                    vTaskResume( pxWriteSDTask );
                                    vTaskResume( pxCountFData );
                                    vTaskResume( pxFreeData );
                                    //vTaskResume( pxLedTask );
                                    vTaskResume( pxADCTask );    
                                }
                            }    
                        } /* Fin de for de Conteo */

                        /* El conteo finalizo sin activaci�n de Modo Auto Baud Rate */
                        if( 1 == auxTime )
                        {
                            /* Env�o de mensaje para indicar que no se activo modo AutoBaudRate */
                            printf("Auto Baud Rate NO Activado... \n");

                            /* Env�o de dato para estado de LEDs */
                            dataStatusLED = 0x90;
                            xQueueSendToBack( xStatusLED, &dataStatusLED, NOT_WAIT_TIME );

                            /* Se Arrancan las Tareas */
                            vTaskResume( pxInitSDTask );
                            vTaskResume( pxWriteSDTask );
                            vTaskResume( pxCountFData );
                            vTaskResume( pxFreeData );
                            //vTaskResume( pxLedTask );
                            vTaskResume( pxADCTask );
                        }  
                    }
                    else
                    {
                        /* Se completa el mensaje de Salida */
                        printf("No Activado... \n");
                    }         
                }
                else                /* Hay presencia de Memoria SD al arrancar ArtLogSD */
                {
                    /* Se completa el mensaje de Salida */
                    printf("No Activado... \n");

                    /* Se Arrancan las Tareas */
                    vTaskResume( pxInitSDTask );
                    vTaskResume( pxWriteSDTask );
                    vTaskResume( pxCountFData );
                    vTaskResume( pxFreeData );
                    //vTaskResume( pxLedTask );
                    vTaskResume( pxADCTask );         
                }
            }
            else
            {
//                /* Mensaje de Entrada */
//                printf( "ARTLOGSD V%1.1f \n", FWVersion );
//
//                /* Env�o de mensaje para verificar notificar Baud Rate Activado  */
//                printf( "Auto Baud Rate Activado... \n" );

                /* Aviso para introducir la memoria */
                if( CD_SW )
                {
                    delay_ms( 200 );
                    if( CD_SW )
                    {
                        printf( "Antes de continuar coloque la memoria microSD ...\n" );   
                    }
                }
                else
                {
                    printf( "Memoria microSD Detectada ...\n" );
                    printf( "Listo para recibir cadenas por Rx ...\n" );
                }

                /* Env�o de dato para estado de LEDs */
                dataStatusLED = 0x91;
                xQueueSendToBack(xStatusLED, &dataStatusLED, NOT_WAIT_TIME);

                /* Se cambia la configuraci�n del pin PRB6 modo Input Capture */
                /* NOTA IMPORTANTE: Proceso cr�tico, se suspenden todas las tareas y
                 * todas las interrupciones debido a cambio de configuraci�n de PIN */

                /* Tasks Suspend */
                /* Las tareas se inicializan en modo Suspedidas */

                /* Timers Stop */
                xTimerStop( xTimerOutFreeD, 0 ); 
                xTimerStop( xTimerOutCD_SW_1, 0 ); 
                xTimerStop( xTimerOutCD_SW_2, 0 ); 
                xTimerStop( xTimerOutLED, 0 );
                xTimerStop( xTimerOutVin, 0 );

                /* Configuraci�n de Inversi�n de RX */
                if ( 1 == datos_a_evaluar.modo_de_baud_rate ){
                    UARTConfigure( UARTx, UART_ENABLE_PINS_TX_RX_ONLY | UART_ENABLE_HIGH_SPEED | UART_INVERT_RECEIVE_POLARITY );
                } 

                /* Deshabilitaci�n de Interrupciones */
                INTEnable( INT_SOURCE_UART_RX( UARTx ), INT_DISABLED );
                INTEnable( INT_IC1, INT_DISABLED );

                    /* INICIO Programacion de PPS */
                    PPSUnLock;
                    /* Input Capture 1 */   
                        PPSInput( 3, IC1, RPB6 );
                    /* FIN Programacion de PPS */
                    PPSLock;

                    /* Estabilizaci�n por configuraci�n */
                    delay_ms(500);      /* 0.5 seg */

                /* Habilitaci�n de Interrupciones */  
                INTEnable( INT_IC1, INT_ENABLED );

                /* Timers Start */
                xTimerStart( xTimerOutFreeD, 0 ); 
                xTimerStart( xTimerOutCD_SW_1, 0 ); 
                xTimerStart( xTimerOutCD_SW_2, 0 ); 
                xTimerStart( xTimerOutLED, 0 );
                xTimerStart( xTimerOutVin, 0 );

                /* Entra a Modo Auto Baud Rate*/
                vMedBitTime();              
            }

            /* Se suspende tarea al terminar validaci�n en modo Auto Baud Rate */
            vTaskSuspend( NULL );
        }
    }
}

/* -------------------------------------------------------------------------- */
/* Nombre de Funci�n:   BusqBaudRate
 * Objetivo:            Se realiza la busqueda de archivo baud.txt */
struct datos_archivo_baud_txt BusqBaudRate( void )
{
    /* Definici�n de variables */
    char filename[]= "BAUD.txt";
    static uint8_t baud_rate_archivotxt[8] = {0,0,0,0,0,0,0,0};
	static uint8_t modo_baud_rate_archivotxt[6] = {0,0,0,0,0,0};
    uint8_t buffread[20] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    uint8_t i;
	uint8_t j;
    uint8_t num_modo_baud_rate_archivotxt;
	uint32_t num_baud_rate_archivotxt;
    uint32_t br;
    bool aux_separar_datos;

    /* Variables de FatFs */
    FRESULT fr;
    FILINFO fno;
    DSTATUS resF;
    uint32_t accessmode1;    
    uint32_t accessmode2;
    
    /* Inicializaci�n de variables */
    accessmode1 = (   FA_OPEN_ALWAYS | FA_WRITE | FA_READ );
    accessmode2 = ( FA_CREATE_ALWAYS | FA_OPEN_ALWAYS | FA_CREATE_NEW | FA_WRITE | FA_READ );
    aux_separar_datos = FALSE;
    num_modo_baud_rate_archivotxt = 0;
	num_baud_rate_archivotxt = 0;
    j=0;
    datos_txt.modo_de_operacion_ArtLogSD = 0;       /* Modo Normal      */
    datos_txt.baud_rate_archivo = 0;                /* Auto Baud Rate   */
    datos_txt.modo_de_baud_rate = 0;                /* No invertido     */
    
    /* Inicializar unidad y montar */
    resF = disk_initialize( (BYTE)0 );
    if ( RES_OK != resF ) disk_initialize( (BYTE)0 );
        
    resF = f_mount( &FatFs, "", (BYTE)0 );
    if ( RES_OK != resF ) f_mount( &FatFs, "", (BYTE)0 );
      
    /* Archivo en busqueda */    
    printf("[ BusqBaudRate] Prueba para Archivo de Baud Rate [%s]...\n",filename);

    /* Funcion de busqueda de archivos de FatFs */
    fr = f_stat(filename, &fno);  
    if ( FR_OK != fr ) f_stat(filename, &fno);  
        
    switch (fr) 
    {
        case FR_OK:
            /* El Archivo existe y se evalua la acci�n a realizar */
            printf("[ BusqBaudRate] El Archivo existe.\n");
            
            fr = f_open( &file2, filename, ( uint8_t )accessmode1 );
            
            if ( FR_OK != fr ) f_open( &file2, filename, ( uint8_t )accessmode1 );

            fr = f_read( &file2, buffread, sizeof( buffread ), &br );
            
            if ( FR_OK != fr ) f_read( &file2, buffread, sizeof( buffread ), &br );
            
//            printf( "Bytes  Read: %u\n", br );
//            printf( "Cadena Read: %s\n", buffread );
            
            /* Se extraen datos */
            for ( i = 0; i < br; i++ ) {
                if ( ( CHAR_COMA != buffread[ i ] ) & ( FALSE == aux_separar_datos ) ){
                    baud_rate_archivotxt[ i ] = buffread[ i ];
                } /* Fin if argumento true */
                else {
                    aux_separar_datos = TRUE;
                } /* Fin if argumento false */

                if ( TRUE == aux_separar_datos ){
                    if ( CHAR_COMA != buffread[ i ] ){
                        modo_baud_rate_archivotxt[ j ] = buffread[ i ];
                        j++;   
                    }
                }		
            } /* Fin for */
            
//            printf( "%s \n", baud_rate_archivotxt );
//            printf( "%s \n", modo_baud_rate_archivotxt );
            
            /* Se elige modo de operaci�n ( Normal o Baud Rate ) */
            if ( ( CHAR_NUMERO_0 <= baud_rate_archivotxt[ 0 ] ) & ( CHAR_NUMERO_9 >= baud_rate_archivotxt[ 0 ] ) ){
                /* Conversi�n de string a uint32_t */
                num_baud_rate_archivotxt = atoi( baud_rate_archivotxt );
                //printf("Baud Rate en uint32_t: %u\n", num_baud_rate_archivotxt );
                
                datos_txt.modo_de_operacion_ArtLogSD = 0;               /* Modo Nomal */
                datos_txt.baud_rate_archivo = num_baud_rate_archivotxt; /* Baud Rate de Archivo */  
            }
            else if ( ( CHAR_A_MAYUSCULA == baud_rate_archivotxt[ 0 ] ) | ( CHAR_A_MINUSCULA == baud_rate_archivotxt[ 0 ] ) ){
                /* Conversi�n a minusculas */
                for ( i = 0; i < sizeof( baud_rate_archivotxt ); i++ ){
                    baud_rate_archivotxt[ i ] = tolower( baud_rate_archivotxt[ i ] );	
                }
                
                //printf("%s\n", baud_rate_archivotxt );

                if( !( strncmp( ETIQUETA_AUTO, baud_rate_archivotxt, 4 ) ) ){
                    printf( "Auto Baud Rate Activado... \n");    
                }
                else {
                    printf( "Cadenas no s�n iguales... \n" );
                }
                
                datos_txt.modo_de_operacion_ArtLogSD = 1;               /* Modo Auto baud rate */
                datos_txt.baud_rate_archivo = 0;                        /* Baud Rate 0 */  
            }
            else {
                printf("Error: dato no valido ... \n");
                
                datos_txt.modo_de_operacion_ArtLogSD = 1;               /* Modo Auto baud rate */
                datos_txt.baud_rate_archivo = 0;                        /* Baud Rate 0 */  
            }
            
            /* Modo de transmisi�n No invertido o Invertido */
            if ( ( CHAR_NUMERO_0 == modo_baud_rate_archivotxt[ 0 ] ) | ( CHAR_NUMERO_1 == modo_baud_rate_archivotxt[ 0 ] ) ){
                /* Conversi�n de string a uint32_t */
                num_modo_baud_rate_archivotxt = atoi( modo_baud_rate_archivotxt );
                //printf("Modo de Transmisi�n Baud Rate: %u\n", num_modo_baud_rate_archivotxt );
                
                datos_txt.modo_de_baud_rate = num_modo_baud_rate_archivotxt;    /* Modo Invertido o No Invertido */
            }
            else if ( ( CHAR_T_MAYUSCULA == modo_baud_rate_archivotxt[ 0 ] ) | ( CHAR_T_MINUSCULA == modo_baud_rate_archivotxt[ 0 ] ) |
                ( CHAR_F_MAYUSCULA == modo_baud_rate_archivotxt[ 0 ] ) | ( CHAR_F_MINUSCULA == modo_baud_rate_archivotxt[ 0 ] ) ){
                /* Conversi�n a minusculas */
                for ( i = 0; i < sizeof( modo_baud_rate_archivotxt ); i++ ){
                    modo_baud_rate_archivotxt[ i ] = tolower( modo_baud_rate_archivotxt[ i ] );	
                }

                //printf("%s\n", modo_baud_rate_archivotxt );

                if ( !( strncmp( ETIQUETA_FALSE, modo_baud_rate_archivotxt, 5 ) ) ){
                    num_modo_baud_rate_archivotxt = 0;
                    //printf("Modo de Transmisi�n Baud Rate: %u\n", num_modo_baud_rate_archivotxt );
                }
                else {
                    if ( !( strncmp( ETIQUETA_TRUE, modo_baud_rate_archivotxt, 4 ) ) ){
                        num_modo_baud_rate_archivotxt = 1;
                        //printf("Modo de Transmisi�n Baud Rate: %u\n", num_modo_baud_rate_archivotxt );
                    }
                    else {
                        num_modo_baud_rate_archivotxt = 0;
                        printf("Error: dato no valido ... \n");	
                    }
                }
                
                datos_txt.modo_de_baud_rate = num_modo_baud_rate_archivotxt;    /* Modo Invertido o No Invertido */
            }
            else {
                datos_txt.modo_de_baud_rate = 0;                                /* No Invertido */
                printf("Error: dato no valido ... \n");	
            }
            
            break;

        case FR_NO_FILE:
            /* El Archivo no existe, se habilita modo Auto Baud Rate */        
            printf("[ BusqBaudRate] El Archivo no existe.\n");
            
            datos_txt.modo_de_operacion_ArtLogSD = 1;               /* Modo Auto baud rate */
            datos_txt.baud_rate_archivo = 0;                        /* Baud Rate 0 */  
            datos_txt.modo_de_baud_rate = 0;                        /* No Invertido */
            
            fr = f_open(&file2, filename, (BYTE)accessmode2);
            printf("[ BusqBaudRate] Archivo Creado: %s \n", filename);
            
            break;

        default:
            printf("[ BusqBaudRate] Error de Archivo... \n");
            
            datos_txt.modo_de_operacion_ArtLogSD = 1;               /* Modo Auto baud rate */
            datos_txt.baud_rate_archivo = 0;                        /* Baud Rate 0 */  
            datos_txt.modo_de_baud_rate = 0;                        /* No Invertido */
            
            fr = f_open(&file2, filename, (BYTE)accessmode2);
            printf("[ BusqBaudRate] Archivo Creado: %s \n", filename);
    }
    
    return datos_txt;
    
}

/* -------------------------------------------------------------------------- */
/* Nombre de la Funci�n:    TimeOutCD_SW
 * Objetivo:                Timer para eliminar rebotes 
 * Observaciones:           */
void TimeOutCD_SW_2( void *pvParameters )
{
    /* Evita warnings del compilador */
    (void) pvParameters;
    
    /* Definici�n de Variables */
    uint32_t n;
    
    n = timerCD_SW_2;
    if (n) timerCD_SW_2 = --n;
    timerBaudRate++;
    //printf("TimerBaudRate: %u\n", timerBaudRate);
}