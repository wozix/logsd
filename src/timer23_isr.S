#include <p32xxxx.h>
#include <sys/asm.h>
#include "ISR_Support.h"

	.set	nomips16
	.set 	noreorder
 	
	.extern vT23InterruptHandler
	.extern xISRStackTop
	.global	vT23InterruptWrapper

	.set	noreorder
	.set 	noat
	.ent	vT23InterruptWrapper

vT23InterruptWrapper:

	portSAVE_CONTEXT
	jal vT23InterruptHandler
	nop
	portRESTORE_CONTEXT

	.end	vT23InterruptWrapper

