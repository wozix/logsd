/* Archivo:         ArtLogSD.h
 * Autor:           Olaf I. G�mez P.
 * Fecha:           22/Octubre/15
 * uC:              PIC32MX170F256B
 * Versi�n:         0
 * Descripci�n:     Configuraci�n de Hardware para ArtLogSD
 *                    
 */

#ifndef ARTLOGSD_H
#define	ARTLOGSD_H

/* Kernel includes. */
#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"

/* Includes */
#include <plib.h>
#include <xc.h>
#include <stdio.h>
#include <ports.h>
#include <stdbool.h>
#include <string.h>
#include "rtBDebug.h"
#include "serial.h"

/* Etiquetas de Sistema */
/* -------------------------------------------------------------------------- */
/* Frecuencia del sistema */
#define SYS_FREQ                    ( 48000000ul )    /* 48 MHz */
/* Frecuencia de perif�ricos */
#define PERIPHERAL_CLOCK            ( 24000000ul )    /* 24 MHz */
/* Frecuencia de Core Timer */
#define FREQ_CORE_TIMER             SYS_FREQ / 2      /* 24 MHz */
/* Conversi�n de Ticks a us*/
#define TICKS_TO_US                 ( FREQ_CORE_TIMER / 1000000ul )

/* Versi�n de FW ArtLogSD */
#define FWVersion                   ( 1.3 )

/* Etiquetas para Tareas */
/* -------------------------------------------------------------------------- */
/* Tama�o de Stack */
#define STACK_SIZE                  configMINIMAL_STACK_SIZE
/* Prioridades de Tareas */
#define ABAUDR_PRIORITY             ( tskIDLE_PRIORITY + 4 )
#define INITSD_PRIORITY             ( tskIDLE_PRIORITY + 3 )
#define WRTSD_PRIORITY              ( tskIDLE_PRIORITY + 2 )
#define COUNTDATA_PRIORITY          ( tskIDLE_PRIORITY + 1 )
#define FREDATA_PRIORITY            ( tskIDLE_PRIORITY + 1 )
#define LEDS_PRIORITY               ( tskIDLE_PRIORITY + 2 )
#define ADC_PRIORITY                ( tskIDLE_PRIORITY + 1 )

/* Etiquetas para LEDs */
/* -------------------------------------------------------------------------- */
/* LED Indicador de Mem�ria SD */
#define SD_LED                      ( ( uint8_t ) 0 )
/* LED Indicador de RX */
#define RX_LED                      ( ( uint8_t ) 1 )

/* Etiquetas Varias */
/* -------------------------------------------------------------------------- */
#define TIME_WRITESD_ADC            ( 10ul )    /* Unidades: [seg] */

/* Tama�o de Filas */
/* -------------------------------------------------------------------------- */
/* Fila para voltaje */
#define QUEUE_LENGTH_STATUSLED      ( ( uint8_t )  5)
#define QUEUE_LENGTH_RXCHARS        ( ( uint8_t ) 18)
#define QUEUE_LENGTH_RXCHARSAUX     ( ( uint8_t )  1)
#define QUEUE_LENGTH_MSNAUTOBR      ( ( uint8_t )  1)

/* Tiempos de espera para las Filas */
/* -------------------------------------------------------------------------- */
/* Fila para funci�n de ADCTask */
#define xQSTB_VoltageADC_BLOCK_TIME ( ( TickType_t ) 0 )
#define xQR_VoltageADC_BLOCK_TIME   ( ( TickType_t ) 0xFFF )

/* Tiempos para xTimer */
#define TIMER_OUT_PERIOD            (    1 / portTICK_PERIOD_MS )
#define TIMER_OUT_LED               ( 1000 / portTICK_PERIOD_MS )
#define TIMER_OUT_Vin               ( 1000 / portTICK_PERIOD_MS )

/* Voltaje m�nimo para mantener el archivo de texto abierto */
#define voltLowVin                  ( (float) 5.0 )

/* Periodo de espera igual a CERO */
#define NOT_WAIT_TIME               (    0 / portTICK_PERIOD_MS  )

/* Periodo de espera de 2 seg */
#define WAIT_TIME_2_seg             ( 2000 / portTICK_PERIOD_MS  )

/* Se define el numero de elementos de los arreglos */
#define serialBUFFER_LEN			( ( UBaseType_t  ) 3*1024 )

/* Se define la cantidad de valores de Bit Time */
#define N_VALUES_BIT_TIME           ( 50ul )

/* Declaraci�n de Tasks */
/* -------------------------------------------------------------------------- */
extern void ABaudRTask  ( void *pvParameters );
extern void InitSDTask  ( void *pvParameters );
extern void WriteSDTask ( void *pvParameters );
extern void FreeData    ( void *pvParameters );
extern void CountFData  ( void *pvParameters );
extern void LedTask     ( void *pvParameters );
extern void ADCTask     ( void *pvParameters );

/* Declaraci�n de funciones */
/* Para Timers */
extern void TimeOut     ( void *pvParameters ); 
extern void TimeOutFreeD( void *pvParameters );
extern void TimeOutCD_SW_1( void *pvParameters );
extern void TimeOutCD_SW_2( void *pvParameters );
extern void TimeOutLED  ( void *pvParameters );
extern void TimeOutVin  ( void *pvParameters );

/* Declaraci�n de Filas*/
/* -------------------------------------------------------------------------- */
QueueHandle_t xStatusLED;
QueueHandle_t xRxedChars;
QueueHandle_t xRxedCharsAux;
QueueHandle_t xMensajeAutoBaudRate;

/* Declaraci�n de Semaphores */
/* -------------------------------------------------------------------------- */
xSemaphoreHandle xFlagFreeData;

/* Declaraci�n de TaskHandle */
/* -------------------------------------------------------------------------- */
TaskHandle_t pxABaudRTask;
TaskHandle_t pxInitSDTask;
TaskHandle_t pxWriteSDTask;
TaskHandle_t pxCountFData;
TaskHandle_t pxFreeData;
TaskHandle_t pxLedTask;
TaskHandle_t pxADCTask;

/* Declaraci�n de TimerHandle */
/* -------------------------------------------------------------------------- */
TimerHandle_t xTimerOutFreeD;
TimerHandle_t xTimerOutCD_SW_1;
TimerHandle_t xTimerOutCD_SW_2;
TimerHandle_t xTimerOutLED;
TimerHandle_t xTimerOutVin;

/* Declaraci�n de Variables */
static char xRxedCharsPar[serialBUFFER_LEN];
static char xRxedCharsParAUX1[serialBUFFER_LEN];
static uint32_t iChar;
static uint32_t cntChar_BaudRate;
static uint8_t cntFraming_Error;
static uint8_t index_baudRates;
bool init_SD_OK;                        /* Bandera para conocer si esta inicializada la SD Card */
static bool flag_autoBaudRate_task;     /* Bandera para conocer si ya se ejecuto la tarea de Auto Baud Rate*/
static bool flag_autoBaudRate;          /* Bandera para conocer si ya se ejecuto el Auto Baud Rate*/

/* Variables para Baud-Rate */
static uint32_t values_bit_time[N_VALUES_BIT_TIME];
static uint32_t aux_values_bit_time[N_VALUES_BIT_TIME];
//static volatile uint32_t timerBaudRate;
 
/* Configuracion de ADC [Funci�n: OpenADC10] */
/* -------------------------------------------------------------------------- */
           /* Turn module on | ouput in integer | trigger mode auto | enable autosample */
#define PARAM1  ADC_MODULE_ON | ADC_FORMAT_INTG | ADC_CLK_AUTO | ADC_AUTO_SAMPLING_ON
       /* ADC ref external    | disable offset test    | disable scan mode | perform 5 samples | use dual buffers | use alternate mode */
#define PARAM2  ADC_VREF_AVDD_AVSS | ADC_OFFSET_CAL_DISABLE | ADC_SCAN_OFF | ADC_SAMPLES_PER_INT_5 | ADC_ALT_BUF_ON | ADC_ALT_INPUT_ON
               /* use ADC internal clock | set sample time */    
#define PARAM3  ADC_CONV_CLK_INTERNAL_RC | ADC_SAMPLE_TIME_15
                /* set AN11 */
#define PARAM4  ENABLE_AN11_ANA 
         /* assign channels to scan */
#define PARAM5  SKIP_SCAN_AN11
/* Periodo de muestreo de voltaje */
#define WAIT_VOLTAGE_FREQUENCY_MS   ( 1000 / portTICK_PERIOD_MS )

/* Periodo de espera tarea de prueba  */
#define WAIT_WRITE2_FREQUENCY_MS    ( 5 / portTICK_PERIOD_MS )

/* Configuracion de Input Capture [Funci�n: vMedBitTime ] */
/* -------------------------------------------------------------------------- */
#define TOGGLES_PER_SEC             1
#define COUNT_TIME                  1   
#define T23_TICK                    ( SYS_FREQ / 2 / 1 / TOGGLES_PER_SEC  ) 

/* Etiquetas de Hardware */
/*----------------------------
 * I/O pins
 *
 *  ArtLogSD cuenta con 2 LEDs 
 *      LED1    MCU pin 2  RA0
 *      LED2    MCU pin 3  RA1
 *
 *  ArtLogSD cuenta con TX/RX RS232
 *      TXout   MCU pin 26 RPB15 
 *      RXin    MCU pin 15 RPB6
 *
 *  ArtLogSD cuenta con SPI para SD Card
 *      MOSI    MCU pin 14 RPB5
 *      MISO    MCU pin 17 RPB8
 *      SCLK    MCU pin 25 RB14
 *      CS      MCU pin 22 RB11
 *      CD      MCU pin 16 RB7
 *
 *----------------------------*/

/* TopOverlay D2 */
#define LED0_TRIS_OUT()         ( TRISACLR = BIT_0 )

/* TopOverlay D3 */	
#define LED1_TRIS_OUT()         ( TRISACLR = BIT_1 )

#define LED0_ON()               LATASET = BIT_0;
#define LED0_OFF()              LATACLR = BIT_0;
#define LED0_INV()              LATAINV = BIT_0;

#define LED1_ON()               LATASET = BIT_1;
#define LED1_OFF()              LATACLR = BIT_1;
#define LED1_INV()              LATAINV = BIT_1;

#define LEDS_ON()               do{ LED0_ON(); LED1_ON(); }while(0);
#define LEDS_OFF()              do{ LED0_OFF(); LED1_OFF(); }while(0);

#define TXout_TRIS()            TRISBCLR = BIT_15

#define RXin_TRIS()             TRISBSET = BIT_6

/* Defines para Hardware */
#define CD_SW                   PORTBbits.RB7

/* Configuraci�n de Hardware */
void vHWInicializacion( void );

/* Funci�n para medir Bit Time de datos en Rx */
void vMedBitTime( void );

/* Configuraci�n de ADC */
void vADCPortInit( void );

/* Cambia de estado el LED indicado */
void vToggleLED( uint8_t uLED );

/* Parpadeo de LEDs */
void vBlinkLED( uint8_t uLED, uint32_t timeBlink );

/* Funci�n de tiempo */
void delay_ms( uint32_t mseg );

/* Funci�n de tiempo */
void delay1ms( void );

/* Funci�n de tiempo */
void delayus( uint32_t usec );

/* Declaraci�n de pending function para Bit Time */
void vProcessBitTime( void  *pvParameter1, uint32_t ulParameter2 );

#endif	/* ARTLOGSD_H */

