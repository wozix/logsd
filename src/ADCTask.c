/* Archivo:         ADCTask.c
 * Autor:           Olaf I. Gómez P.
 * Fecha:           16/Diciembre/15
 * uC:              PIC32MX170F256B
 * Versión:         0
 * Descripción:     Archivo para Task "ADCTask"
 *                    
 */

/* App Include */
#include "ArtLogSD.h"

static volatile uint32_t timerVin;

/* -------------------------------------------------------------------------- */
/* Nombre de Función:   vADCPortInit
 * Objetivo:            Configuración de ADC. */
void vADCPortInit( void )
{
    /*  Se apagan los canales de ADC y deshabilita interrupciones */
    CloseADC10();                       
    /*  Configuración de las entradas del ADC MUX */
    SetChanADC10(ADC_CH0_POS_SAMPLEA_AN11);
    /* Configuración de parametros */
    OpenADC10(PARAM1, PARAM2, PARAM3, PARAM4, PARAM5);
}

/* -------------------------------------------------------------------------- */
/* Nombre de la Tarea:  ADCTask
 * Objetivo:            Controla la lectura de ADC. 
 *                      Voltaje de entrada [24V-12V]. */
void ADCTask ( void *pvParameters )
{
    /* Solo para no tener warnings */
    ( void ) pvParameters;
    
    /* Declaraciones */
    TickType_t xTimeToWait;
    uint8_t dataStatusLED;              /* Estado de LEDs */
    uint8_t *envio_datosADC;            /* Puntero para envio de datos de ADC */
    uint8_t texto_vin[7];               /* Buffer para almacenar Vin */
    uint32_t channel_11;                /* Lectura de la conversión */
    float voltaje;                      /* Voltaje entrada */
    float cVolIn;                       /* Conversión de Voltaje  */

    /* Inicialización de variables */
    xTimeToWait = xTaskGetTickCount();  /* Return: The count of ticks since vTaskStartScheduler was called */
    uint8_t mensaje_Vnormal[] = "[~~~]o \r\n\r\n\0";
    uint8_t mensaje_Vbajo  [] = "[~~ ]o \r\n\r\n\0";
    uint8_t mensaje_Vminimo[] = "[~  ]o \r\n\r\n\0";
    uint8_t mensaje_Vcero  [] = "[   ]o \r\n\r\n\0";
    const float volt_normal = 11.5;     /* Voltaje para determinar nivel: Normal */
    const float volt_bajo   =  8.0;     /* Voltaje para determinar nivel: Bajo */
    const float volt_minimo =  4.0;     /* Voltaje para determinar nivel: Minimo */
    timerVin = TIME_WRITESD_ADC;
        
    for( ;; )
    {        
        /* Periodo de tiempo para la obtención de datos del voltaje  */
        vTaskDelayUntil( &xTimeToWait, WAIT_VOLTAGE_FREQUENCY_MS );
        
        /* Declaracion de etiquetas para envio a SD Card*/
        uint8_t mensaje_Vin_1[] = "\r\nNotificaciones_ArtLogSD        - +\n";
        uint8_t mensaje_Vin_2[] = "[Voltaje:] ";
        uint8_t mensaje_Vin_3[] = "V [Status:]  ";
        
        /* Habilitación de ADC */
        EnableADC10();
    
        /* Esperar para la primera conversión completa y tener datos en los registros */
        while( !mAD1GetIntFlag() ) { } 

        /* Se obtiene el valor de la conversión del registro 0 */
        channel_11 = ReadADC10(0);
        
        /* Proceso de conversión de voltaje */
        /* Conversión de voltaje de entrada */
        voltaje = ((3.3 * channel_11)/1024);
        /* Conversión a voltaje real */
        cVolIn = voltaje / (float) (0.046720);   /* Factor de Ajuste Anterior: 0.047619 */
        
        /* Envio de dato para estados de LEDs */
        if( cVolIn < voltLowVin ) dataStatusLED = 0x81;
        else dataStatusLED = 0x82;
        
        xQueueSendToBack( xStatusLED, &dataStatusLED, NOT_WAIT_TIME ); 
        
        /* Si esta presenta la memoria e inicializada se manda a escribir en la 
         * SD Card el valor de Vin */
        if( ( !CD_SW ) & ( init_SD_OK == TRUE ) & ( 0 == timerVin ) )
        {
            /* Armado de cadena de voltaje */      
            sprintf(texto_vin,"%2.3f",cVolIn);
            texto_vin[7] = 0x00;       /* Caracter NULL (\0) */

            /* Concatenación de cadenas */
            /* strcat( <cadena_destino>, <cadena_fuente> ) */
            strcat( mensaje_Vin_1, mensaje_Vin_2 );
            strcat( mensaje_Vin_1, texto_vin );
            strcat( mensaje_Vin_1, mensaje_Vin_3 );

            if(      volt_normal <= cVolIn ) {strcat( mensaje_Vin_1, mensaje_Vnormal );}
            else if(   volt_bajo <= cVolIn ) {strcat( mensaje_Vin_1, mensaje_Vbajo );}
            else if( volt_minimo <= cVolIn ) {strcat( mensaje_Vin_1, mensaje_Vminimo );}
            else {strcat( mensaje_Vin_1, mensaje_Vcero );}         

            envio_datosADC = pvPortMalloc( strlen( mensaje_Vin_1 ) + 1 );    
            memcpy( envio_datosADC,( uint8_t *)mensaje_Vin_1, strlen( mensaje_Vin_1 ) );
            envio_datosADC[ strlen( mensaje_Vin_1 ) ] = 0x00;      /* Caracter NULL (\0) */

            /* Envío de cadena de datos para impresión de valor de voltaje por xTiempo */
            if (xQueueSendToBack( xRxedChars, &envio_datosADC, xQSTB_VoltageADC_BLOCK_TIME )== pdFALSE)
            {
                printf( "Error al Escribir en Queue [xRxedChars] ******** \n" );
                vPortFree(envio_datosADC);
            }
            
            /* Se reinicia conteo de xTiempo */
            timerVin = TIME_WRITESD_ADC;
        }  
    }
}


/* -------------------------------------------------------------------------- */
/* Nombre de la Función:    TimeOutVin
 * Objetivo:                Timer para guardar valor de Vin en SD Card
 * Observaciones:           */
void TimeOutVin( void *pvParameters )
{
    /* Evita warnings del compilador */
    (void) pvParameters;
    
    /* Definición de Variables */
    uint32_t n;
    
    n = timerVin;
    if (n) timerVin = --n;
}