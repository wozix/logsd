/* Archivo:         WriteSDTask.c
 * Autor:           Olaf I. G�mez P.
 * Fecha:           16/Diciembre/15
 * uC:              PIC32MX170F256B
 * Versi�n:         0
 * Descripci�n:     Archivo para Task "WriteSDTask"
 *                    
 */

/* App Include */
#include "memoryCard.h"
#include "serial.h"
#include "rtBDebug.h"

/* -------------------------------------------------------------------------- */
/* Nombre de la Tarea:  WriteSDTask
 * Objetivo:            Escribir en memoria SD Card
 * Observaciones:        */
void WriteSDTask ( void *pvParameters )
{
    /* Solo para no tener warnings */
    ( void ) pvParameters;
    
    /* Definiciones */
    uint8_t *dataTask;
    uint32_t contCadenas;
    char filename[]= "LGSD0000.txt";
    
    /* Variables de FatFs*/
    FRESULT FOpenRes;
    uint32_t numread;
    
    /* Inicializaci�n */
    numread = 0;
    contCadenas = 0;
      
    for( ;; )
    {
        /* L�nea para Run-Time Behavior */
        if( 1 == LABEL_RTB_DEBUG ) rtB_debug( WRTSD_OFF );
        
            /* Se extraen los datos de la fila */
            xQueueReceive( xRxedChars, &dataTask, portMAX_DELAY );
        
        /* L�nea para Run-Time Behavior */
        if( 1 == LABEL_RTB_DEBUG ) rtB_debug( WRTSD_ON );
          
            /* Se almacena la informaci�n de la fila */
            FOpenRes = f_write(&file1, dataTask, strlen(dataTask), &numread);
            printf("[ WriteSDTask ] Bytes Guardados: %u\n", numread );          
            f_sync( &file1 );
            
            /* Se libera memoria din�mica */
            vPortFree(dataTask);        /* Se libera memoria*/
                     
            /* Se evalua el tama�o del archivo */
            if( f_size(&file1) >= maxSizeFile )
            {
                /* Close file */
                FOpenRes = f_close(&file1);

                if(!FOpenRes)
                {
                    printf("[ WriteSDTask ] Archivo cerrado.\n");
                }
                else
                {
                    printf("[ WriteSDTask ] Falla en cierre de Archivo... error = %u \n",FOpenRes);
                }

                /* Se incrementa el indice del Archivo */
                countFile++;

                /* Se llama a la funcion para nuevo nombre de archivo */
                newNameFile( countFile, filename );

                /* Se abre archivo nuevo */
                FOpenRes = f_open (&file1, filename, FA_CREATE_ALWAYS | FA_OPEN_ALWAYS | FA_CREATE_NEW | FA_WRITE |FA_READ);
                printf("[ WriteSDTask ] Archivo Creado: %s \n", filename);

                /* Unmount Filesystem */
                //f_mount((void *)0,"",0);
            }

            /* Checar espacio libre en SD Card */
            valFreeS = GetFreeSpace();
            if( valFreeS <= minSpaceSDCard  )
            {
                printf( "[ WriteSDTask ] Memoria SD Card Llena...\n" );
                printf( "[ WriteSDTask ] Vaciar Memoria SD Card...\n" );

                /* Unmount Filesystem */
                f_mount((void *)0,"",0);

                /* Deshabilitaci�n de Interrupci�n para evitar recibir datos por UART */
                INTEnable( INT_SOURCE_UART_RX( UARTx ), INT_DISABLED );
            }
    }
}