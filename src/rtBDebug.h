/* Archivo:         rtBDebug.h
 * Autor:           Olaf I. G�mez P.
 * Fecha:           29/Enero/16
 * uC:              PIC32MX170F256B
 * Versi�n:         0
 * Descripci�n:     Definici�n de Funciones para Run-Time Behavior Debug
 *                    
 */

/* SISTEMA RUN-TIME BEHAVIOR
 * 
 * Para poder monitorear la duraci�n de una Tarea, Interrupci�n, Funci�n etc, se
 * debe enviar un Byte de informaci�n con la siguiente estructura:
 * 
 * B7 | B6 | B5 | B4 | B3 | B2 | B1 | B0
 * -------------------------------------
 *  1 |1/0 |  0    0    0    0    0    0
 * 
 * B7 Es un bit fijo a 1
 * B6 Indica inicio y fin [Inicio = 1  Fin = 0]
 * B4-B0 Indica n�mero de Tarea, Interrupci�n, Funci�n etc, de acuerdo a la 
 * Tabla siguiente:
 * 
 * B4 | B3 | B2 | B1 | B0
 * ----------------------
 *  0    0    0    0    0    Proceso 0
 *  0    0    0    0    1    Proceso 1
 *  0    0    0    1    0    Proceso 2
 *  0    0    0    1    1    Proceso 3
 *  0    0    1    0    0    Proceso 4
 *  0    0    1    0    1    Proceso 5
 *  .                   .
 *  .                   .
 *  .                   .
 *  1    0    1    1    1    Proceso 23
 * 
 */

#ifndef RTBDEBUG_H
#define	RTBDEBUG_H

/* Includes */
#include <stdint.h>
#include <plib.h>

/* Defines */
/* La etiqueta LABEL_RTB_DEBUG = 1 habilita la funcionalidad de Run-Time si  
 * LABEL_RTB_DEBUG = 0 se deshabilita
 */
#define LABEL_RTB_DEBUG     0

/* Byte para indicar Inicio de Proceso */
#define INITSD_ON           (0xC0)
#define WRTSD_ON            (0xC1)
#define LEDS_TASK_ON        (0xC2)

/* Byte para indicar Fin de Proceso */
#define INITSD_OFF          (0x80)
#define WRTSD_OFF           (0x81)
#define LEDS_TASK_OFF       (0x82)

/* Funci�n Run-Time Behavior FreeRTOS */
void rtB_debug( uint8_t data );

#endif	/* RTBDEBUG_H */

