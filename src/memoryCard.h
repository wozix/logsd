/* Archivo:         memoryCard.h
 * Autor:           Olaf I. G�mez P.
 * Fecha:           22/Octubre/15
 * uC:              PIC32MX150F128B
 * Versi�n:         0
 * Descripci�n:     Configuraci�n, inicializaci�n de Memory Card (Tasks)
 *                    
 */

#ifndef MEMORYCARD_H
#define	MEMORYCARD_H

/* Scheduler include files. */
#include "FreeRTOS.h"
#include <integer.h>

/* 'Include' de la app */
#include "ArtLogSD.h"

/* SD Card include files */
#include "diskio.h"
#include "ff.h"


/* Etiquetas para el manejo de FatFs */
/* -------------------------------------------------------------------------- */
/* Tama�o de Archivo de texto (Bytes)        MB    KB    Bytes */
#define maxSizeFile                 ((DWORD) 10 * 1024 * 1024 )
//#define maxSizeFile                 ((DWORD)       512 * 1024 )
/* Espacio libre para dejar de guardar datos (KBytes) */
#define minSpaceSDCard              ((DWORD) 100 * 1024 )

/* Variables para el manejo de FatFs */
/* -------------------------------------------------------------------------- */
FATFS FatFs;			/* File system object */
FIL file1, file2;		/* File objects */

uint32_t countFile;
DWORD valFreeS;

/* Prototipo de funciones */
/* -------------------------------------------------------------------------- */
/* Funci�n para busqueda de nuevo archivo */
void BusqArchivo( void );

/* Funci�n para generar nuevo nombre de archivo */
void newNameFile( uint32_t Buffp, char file2name[] );

/* Para SD Card */
DWORD GetFreeSpace( void );

/* Funci�n para imprimir el monitoreo de resultados de FatFs */
static void put_rc( FRESULT rc );

 /* Funci�n para abrir o crear un archivo en "append mode". */
FRESULT open_append( FIL* fp, const char* path );

#endif	/* MEMORYCARD_H */

