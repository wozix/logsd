/* Archivo:         LedTask.c
 * Autor:           Olaf I. G�mez P.
 * Fecha:           16/Diciembre/15
 * uC:              PIC32MX170F256B
 * Versi�n:         0
 * Descripci�n:     Archivo para Task "LedTask"
 *                    
 */

/* App Include */
#include "ArtLogSD.h"
#include "AutoBaudRate.h"

static volatile uint32_t timerLED;

/* -------------------------------------------------------------------------- */
/* Nombre de la Tarea:  LedTask
 * Objetivo:            Controla el funcionamiento de los LEDs. */
void LedTask ( void *pvParameters )
{
    /* Solo para no tener warnings */
    ( void ) pvParameters;
    
    /* Definici�n */
    uint8_t dataStatusLED;
    bool vol_in_OK;
    bool start_RX;
    bool cnt_RX;
    bool modo_AutoBaudRate;
    
    /* Inicializacion */
    init_SD_OK = FALSE;
    start_RX = FALSE;
    cnt_RX = FALSE;
    vol_in_OK = TRUE;
    modo_AutoBaudRate = FALSE;
    
    for( ;; )
	{
        /* L�nea para Run-Time Behavior */
        if( 1 == LABEL_RTB_DEBUG ) rtB_debug( LEDS_TASK_OFF );
          
        /* Se extraen los datos de la fila */
        if( xQueueReceive( xStatusLED, &dataStatusLED, 500 / portTICK_PERIOD_MS ) == pdTRUE )
        {
            /* L�nea para Run-Time Behavior */
            if( 1 == LABEL_RTB_DEBUG ) rtB_debug( LEDS_TASK_ON );
        
            switch( dataStatusLED )
            {
                /* Bajo Voltaje de entrada Vin */
                case 0x81:
                    LEDS_OFF();
                    vol_in_OK = FALSE;
                    break;
                /* Voltaje de entrada Vin OK */
                case 0x82:
                    vol_in_OK = TRUE;
                    break;    
                /* Inicializaci�n correcta de SD */    
                case 0xB0:
                    LED0_ON();
                    init_SD_OK = TRUE;
                    break;
                /* Recepci�n de datos por RX */    
                case 0xC0:
                    timerLED = 35;
                    start_RX = TRUE;
                    cnt_RX = TRUE;
                    /* Se evita encender el LED cuando no hay voltaje en Vin */
                    if( vol_in_OK == TRUE )
                    {
                        /* Blink de 5ms*/
                        vBlinkLED( RX_LED, 5 );
                    }
                    break;
                /* Modo Auto Baud Rate Desactivado*/
                case 0x90:
                    modo_AutoBaudRate = FALSE;
                    break;
                /* Modo Auto Baud Rate Activado*/
                case 0x91:
                    modo_AutoBaudRate = TRUE;
                    /* Leds para indicar que el FW entra en Modo Auto Baud Rate */
                    vBlinkLED( SD_LED, 500 );
                    vBlinkLED( RX_LED, 500 );
                    break;                   
                default:
                    LEDS_OFF();
            }
            
            /* Manejo de LED1 */
            if( (init_SD_OK == TRUE) & (vol_in_OK == TRUE) & (start_RX == TRUE) )
            {  
                if( 30 <= timerLED )
                {
                    LED1_ON();
                }
            }
            
            start_RX = FALSE;
        }
        else
        {
            /* L�nea para Run-Time Behavior */
            if( 1 == LABEL_RTB_DEBUG ) rtB_debug( LEDS_TASK_ON );
            
            if( FALSE == modo_AutoBaudRate )
            {
                /* Manejo de LED0 */
                if( CD_SW )
                {
                    LED0_OFF();

                    init_SD_OK = FALSE;
                    start_RX = FALSE;
                    cnt_RX = FALSE;

                    /* Se activa la tarea de InitSDTask */
                    vTaskResume( pxInitSDTask );

                    /* Tiempo de Espera */
                    vTaskDelay(WAIT_TIME_2_seg);
                }
                else
                {
                    if( (init_SD_OK == TRUE) & (vol_in_OK == TRUE ) ) LED0_ON();
                }

                /* Manejo de LED1 */
                if( (init_SD_OK == TRUE) & (vol_in_OK == TRUE) & (start_RX == FALSE) & (cnt_RX == TRUE) )
                {  
                    if( 30 <= timerLED )
                    {
                        LED1_ON();
                    }
                    else
                    {
                        vBlinkLED( RX_LED, 100 );
                    }
                }
            }
            else
            {
                /* Leds para indicar que el FW esta en Modo Auto Baud Rate */
                vBlinkLED( SD_LED, 100 );
                vBlinkLED( RX_LED, 100 );
                
                /* Rutina para salir de modo Auto Baud Rate ( Acci�n: Retirar 
                 * la memoria SD )*/
                if( CD_SW )
                {
                    delay_ms( 200 );    /* Espera ante posibles rebotes */
                    if( CD_SW )
                    {
                        /* Se deshabilita la interrupci�n de Input Capture */
                        INTEnable( INT_IC1, INT_DISABLED );      

                        printf( "Antes de continuar coloque la memoria microSD ...\n" );
                        delay_ms( 2000 );    /* 2 seg */
                    }
                }
                else
                {
                    delay_ms( 200 );    /* Espera ante posibles rebotes */
                    if( !CD_SW )
                    {
                        /* Se habilita la interrupci�n de Input Capture */
                        INTEnable( INT_IC1, INT_ENABLED );      
                        
                        printf( "Memoria microSD Detectada ...\n" );
                        delay_ms( 2000 );    /* 2 seg */                      
                    }
                }
            }
        }        
    }
}

/* -------------------------------------------------------------------------- */
/* Nombre de Funci�n:   vToggleLED
 * Objetivo:            Cambia estado de LED. */
void vToggleLED( uint8_t uLED )
{	    
    switch( uLED ) 
    {
        case 0:
            LED0_INV();
            break;
        case 1:
            LED1_INV();
            break;   
        default:
            LEDS_OFF();
    }
}

/* -------------------------------------------------------------------------- */
/* Nombre de Funci�n:   vBlinkLED
 * Objetivo:            Se ejecuta un parpadeo por xTiempo  */
void vBlinkLED( uint8_t uLED, uint32_t timeBlink )
{	    
    switch( uLED ) 
    {
        case 0:
            LED0_OFF();
            delay_ms( timeBlink );
            LED0_ON();
            delay_ms( timeBlink );
            LED0_OFF();
            break;
        case 1:
            LED1_OFF();
            delay_ms( timeBlink );
            LED1_ON();
            delay_ms( timeBlink );
            LED1_OFF();
            break;
        default:
            LEDS_OFF();
    }
}

/* -------------------------------------------------------------------------- */
/* Nombre de la Funci�n:    TimeOutLED
 * Objetivo:                Timer para LEDs
 * Observaciones:           */
void TimeOutLED( void *pvParameters )
{
    /* Evita warnings del compilador */
    (void) pvParameters;
    
    /* Definici�n de Variables */
    uint32_t n;
    
    n = timerLED;
    if (n) timerLED = --n;
}