/* Archivo:         ArtLogSD.c
 * Autor:           Olaf I. G�mez P.
 * Fecha:           22/Octubre/15
 * uC:              PIC32MX170F256B
 * Versi�n:         0
 * Descripci�n:     Configuraci�n de Hardware para ArtLogSD
 *                    
 */

/* App Include */
#include "ArtLogSD.h"
#include "AutoBaudRate.h"
#include "memoryCard.h"

static uint8_t index_valuesBT;
extern uint32_t timerBaudRate;

/* -------------------------------------------------------------------------- */
/* Manejador de la interrupcion de Timer23. IPL0AUTO no tiene efecto, ya que es 
 * usado por FreeRTOS. 
 * La prioridad se configura con el llamado a ConfigIntTimer23. */
void __attribute__( (interrupt(IPL0AUTO), vector(_TIMER_23_VECTOR))) vT23InterruptWrapper( void );

/* -------------------------------------------------------------------------- */
/* Manejador de la interrupcion de Input-Capture 1. IPL0AUTO no tiene efecto, ya que es 
 * usado por FreeRTOS. 
 * La prioridad se configura con el llamado a ConfigIntTimer1. */
void __attribute__( (interrupt(IPL0AUTO), vector(_INPUT_CAPTURE_1_VECTOR))) vIC1InterruptWrapper( void );


/* -------------------------------------------------------------------------- */
/* Nombre de Funci�n:   vHWInicializacion
 * Objetivo:            Configurar todos los puertos de Entrada/Salida de la 
                        tarjeta ArtLogSD. */
void vHWInicializacion( void )
{
    /* Deshabilitacion de JTAG. */
    CFGCONbits.JTAGEN = 0;      
   
    /* Pin I_O y Pin ADC*/
    ANSELACLR = BIT_0;          /* Pin I_O */
    ANSELACLR = BIT_1;          /* Pin I_O */
   
    ANSELBCLR = BIT_14;         /* Pin I_O */
    ANSELBCLR = BIT_15;         /* Pin I_O */
    ANSELBSET = BIT_13;         /*   ADC   */
      
	/* LEDs */
    LED0_TRIS_OUT();            /* Salida  */
    LED1_TRIS_OUT();            /* Salida  */
    
    TRISBCLR = BIT_15;          /* Salida */
    
    LEDS_OFF();                 /* Limpiar LEDs      */
    
    /* INICIO Programacion de PPS */
    PPSUnLock;
    /* RS232 */
    PPSInput( 3, U1RX, RPB6 );
    //PPSInput( 3, IC1, RPB6 );
    PPSOutput( 1, RPB15, U1TX );
              
    /* SPI 1 */
    PPSOutput( 2,RPB5,SDO1 );
    PPSInput( 2, SDI1, RPB8 );
    
    /* FIN Programacion de PPS */
    PPSLock;
    
    /* TX y RX RS232 */
    TXout_TRIS();               /* Salida  */
    RXin_TRIS();                /* Entrada */     
    
    /* Limpiar puertos de RX y TX */
    LATBCLR = BIT_6;
    LATBSET = BIT_15;

    /* SPI */
    TRISBCLR = BIT_5;           /* MOSI Output */
    TRISBSET = BIT_8;           /* MISO Input  */
    TRISBCLR = BIT_14;          /* CLK  Output */
    
    /* SPI Cont... */
    TRISBCLR = BIT_11;          /* Enable Output */    
    LATBSET  = BIT_11;          /* Inicializacion de Enable SPI */
    
    TRISBSET = BIT_7;           /* Card Detected */
    
    /* ADC */
    TRISBSET = BIT_13;          /* AN24V */
    
}

/* -------------------------------------------------------------------------- */
/* Nombre de Funci�n:   vMedBitTime
 * Objetivo:            Medici�n de Bit Time para determinar posible Baud Rate 
                        para RX. */
void vMedBitTime( void )
{
    /* Variables */
    
//    /* Configuraci�n de Timer 3 */
//    //OpenTimer23( T23_ON | T23_SOURCE_INT | T23_PS_1_1, T23_TICK );
//    OpenTimer23( T23_ON | T23_PS_1_1, T23_TICK );
//       
//    /* Configuraci�n de la prioridad del Timer3 */
//    //ConfigIntTimer23(T23_INT_ON | configMAX_SYSCALL_INTERRUPT_PRIORITY + 1);
//    
//    /* Se limpia Bandera de Interrupci�n de Input Capture 1 */
//    mIC1ClearIntFlag();
//      
//    /* Habilitaci�n de Input Capture 1 
//     * - Captura :          EVERY_EDGE
//     * - Interrupci�n:      Habilitada
//     * - Timer:             3
//     * - Tipo de Captura:   Rising Edge 
//     */
//    OpenCapture1( IC_EVERY_EDGE | IC_INT_1CAPTURE | IC_CAP_32BIT | IC_TIMER2_SRC | IC_FEDGE_RISE | IC_ON );
//    ConfigIntCapture1( IC_INT_ON | 0x02 );
    
    OpenTimer23( T23_ON | T23_32BIT_MODE_ON | T23_PS_1_1, T23_TICK );
    
    /* Se limpia Bandera de Interrupci�n de Input Capture 1 */
    mIC1ClearIntFlag();
    
    /* Habilitaci�n de Input Capture 1 
     * - Captura :          EVERY_EDGE
     * - Interrupci�n:      Habilitada
     * - Timer:             3
     * - Tipo de Captura:   Rising Edge 
     */
    OpenCapture1( IC_EVERY_EDGE | IC_INT_1CAPTURE | IC_CAP_32BIT | IC_TIMER2_SRC | IC_FEDGE_RISE | IC_ON );
    
    /* Configuraci�n de prioridad */
    INTEnable(INT_IC1, INT_ENABLED);
    INTSetVectorPriority(INT_INPUT_CAPTURE_1_VECTOR, INT_PRIORITY_LEVEL_3);
    INTSetVectorSubPriority(INT_INPUT_CAPTURE_1_VECTOR, INT_SUB_PRIORITY_LEVEL_0);
    
    /* Inicializaci�n de variables */
    index_valuesBT = 0;
   
}

/* -------------------------------------------------------------------------- */
/* Nombre de Funci�n:   delay_ms
 * Objetivo:            Delay para ms */
void delay_ms( uint32_t mseg )
{    
    do
    {
        mseg --;
        delay1ms();
    }while( mseg );    
}

/* -------------------------------------------------------------------------- */
/* Nombre de Funci�n:   delay1ms
 * Objetivo:            Delay para 1 ms */
void delay1ms( void )
{
    /* Variables */
    uint32_t taux, stop, usec;
    
    /* Declaraciones */
    usec = 1000;            /* 1 ms */

    /* Se calcula el n�mero de Ticks para 1000 usec */
	stop = usec * ( FREQ_CORE_TIMER / 1000000); 

	/* Se obtiene el tick-time y se agrega el intervalo de tick a contar */
	stop += _mfc0(_CP0_COUNT, _CP0_COUNT_SELECT); 

    /* Se espera a llegar al conteo deseado */
	while( 1 ) 
	{ 
		taux = _mfc0(_CP0_COUNT, _CP0_COUNT_SELECT); 
		if(taux >= stop) break; 
	} 
}

/* -------------------------------------------------------------------------- */
/* Nombre de Funci�n:   delayus
 * Objetivo:            Delay para us */
//void delayus( uint32_t usec )
//{
//    /* Variables */
//    uint32_t taux, stop;
//    
//    /* Declaraciones */
//
//    /* Se calcula el n�mero de Ticks para 1000 usec */
//	stop = usec * ( FREQ_CORE_TIMER / 1000000); 
//
//	/* Se obtiene el tick-time y se agrega el intervalo de tick a contar */
//	stop += _mfc0(_CP0_COUNT, _CP0_COUNT_SELECT); 
//
//    /* Se espera a llegar al conteo deseado */
//	while( 1 ) 
//	{ 
//		taux = _mfc0(_CP0_COUNT, _CP0_COUNT_SELECT); 
//		if(taux >= stop) break; 
//	} 
//}

void delayus( uint32_t usec )
{
    /* Variables */
    uint32_t backupCount, targetCount;
    
    /* Declaraciones */    
	targetCount = (usec * TICKS_TO_US);
    backupCount = ReadCoreTimer();
    WriteCoreTimer(0);
    
    /* Se espera a llegar al conteo deseado */
	while( ReadCoreTimer() < targetCount ) ;
    
    WriteCoreTimer( backupCount + targetCount );
}

/* -------------------------------------------------------------------------- */
/* Interrupci�n:        TIMER3
 * Objetivo:            C�digo que se debe ejecutar al ocurrir la interrupci�n
 *                      del TIMER3. */
void vT23InterruptHandler( void )
{
    
    //printf(" Int T3 \n");
    
    LED0_INV();
    
    /* Se limpia Bandera de Interrupci�n */
    mT23ClearIntFlag();
    
}

/* -------------------------------------------------------------------------- */
/* Interrupci�n:        Input Capture 1
 * Objetivo:            C�digo que se debe ejecutar al ocurrir la interrupci�n
 *                      del IC1. */
void vIC1InterruptHandler( void )
{
    /* Variables */
    static portBASE_TYPE xHigherPriorityTaskWoken;
    
    /* Definiciones */
	xHigherPriorityTaskWoken = pdFALSE;
    
    /* Inicio de conteo de tiempo para el proceso de Auto Baud Rate */
    if( 0 == index_valuesBT ) timerBaudRate = 0;
    
    /* Se toma valor de Timer23 */   
    values_bit_time[ index_valuesBT ] = mIC1ReadCapture();            
    index_valuesBT++;
   
    /* Se envia informaci�n a Pending Function para determinar Baud Rate */    
    if( index_valuesBT > N_VALUES_BIT_TIME )
    {
        memcpy( aux_values_bit_time, values_bit_time, N_VALUES_BIT_TIME * 4 );  /* El Valor *4 es por unit32_t (4 Bytes) */              
        if( !( xTimerPendFunctionCallFromISR( vProcessBitTime, (void  *) aux_values_bit_time, N_VALUES_BIT_TIME, &xHigherPriorityTaskWoken) ) )
        {   
            printf("Error xTimerPend [Bit Time]... **************************\n");
        }            
        index_valuesBT = 0;         /* Contador igual a cero */ 
        //timerBaudRate = 0;          /* Inicio de tiempo de conversi�n */
        
        /* Deshabilitaci�n de la interrupci�n */
        INTEnable( INT_IC1, INT_DISABLED );      
    }
    
    /* Se limpia Bandera de Interrupci�n de Input Capture 1 */
    mIC1ClearIntFlag();
    
    /* If sending or receiving necessitates a context switch, then switch now. */
    portEND_SWITCHING_ISR( xHigherPriorityTaskWoken );
}

/* -------------------------------------------------------------------------- */
/* Nombre de la Funci�n:    vProcessBitTime
 * Objetivo:                Pending Funtion para determinar Bit Time
 *                          
 * Observaciones:        */
void vProcessBitTime( void  *pvParameter1, uint32_t ulParameter2 )
{
    /* Variables Generales */
    uint32_t n_values_bit_time;
    uint32_t aux_val_bit_time;
    uint32_t Baud_Rate;
    uint32_t *val_bruto_bit_time;
    uint32_t val_bit_time[ ulParameter2 ];
    uint32_t i;
    uint32_t j;   
    uint32_t Bit_Time_max_ocurrencia;
        
    /* Variables de mensaje para guardar valor de Baud Rate en SD Card */
    static uint8_t mensaje_BaudRate_1[] = "\r\nNotificaciones ArtLogSD      ---> Rx\n";
    static uint8_t mensaje_BaudRate_2[] = "[Baud Rate:] ";
    static uint8_t mensaje_BaudRate_3[] = "bps [Detecci�n en:] ";
    static uint8_t mensaje_BaudRate_4[] = "seg \r\n\r\n\0";

	uint8_t texto_baud_rate[] = { 0, 0, 0, 0, 0, 0, 0, 0 };         /* Buffer para almacenar Baud Rate */
    /*                             Dato de tiempo de Auto Baud Rate */
    /*                                 1  2  3  4  .  1  2  3 \0    */
    uint8_t texto_time_baud_rate[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };         /* Buffer para almacenar Baud Rate */

    uint8_t *envio_msn_Buad_Rate;            /* Puntero para envio de mensaje */
    
	/* Es de tama�o 3 ya que solo se desean 3 valores m�nimos */
	uint32_t valores_OK_bit_time[3][2] = { {0,0},
										   {0,0},
										   {0,0} };
    uint8_t Bit_Time_max_index;
    uint8_t dataStatusLED;
    
    float data_TimeBaudRate;
    
    /* Variables de FatFs */
    FRESULT FOpenRes;
    uint32_t numread;
    uint32_t accessmode3;
    char filename[]= "BAUD.txt";
    
    /* Inicializaci�n de Variables */
    n_values_bit_time = ulParameter2;
    
    printf( "n_values_bit_time: %u \n", n_values_bit_time );
     
    /* Se verifica tama�o de Heap para checar si hay espacio */
    if( xPortGetFreeHeapSize() >= 1000 )
    {
        val_bruto_bit_time = pvPortMalloc(  n_values_bit_time * 4 );
        memcpy( val_bruto_bit_time, (uint32_t *) pvParameter1,  n_values_bit_time * 4 );
    }
    
//    /* Printf de Prueba -ELIMINAR- */
//    for( i = 0; i < n_values_bit_time; i++ )
//    {
//        printf( "%u\n", val_bruto_bit_time[i] );
//    }  
   
    /* C�lculo de tiempos */
    for( i = 1; i < n_values_bit_time; i++ )
    {
        val_bit_time[ i - 1] = val_bruto_bit_time[ i ] - val_bruto_bit_time[ i - 1 ];
    }
    
    /* Los valores de Bit Time se ordenan de menor a mayor y buscar el menor valor con 
     * mayores repeticiones con este valor se calcula el Baud Rate */  
    
    /* M�todo de la Burbuja para ordenar de menor a mayor */
    for ( i = 1; i <= n_values_bit_time - 1; i++ )
    {
    	for( j = 0; j <= ( n_values_bit_time - 2 ); j++ )
    	{
    		if( val_bit_time[ j ] > val_bit_time[ j+1 ]  )
    		{
    			aux_val_bit_time = val_bit_time[ j ];
    			val_bit_time[ j ] = val_bit_time[ j+1 ];
    			val_bit_time[ j+1 ] = aux_val_bit_time;
    		}
    	}
    }
    
//    /* C�lculo de tiempos */
//    for( i = 0; i < n_values_bit_time; i++ )
//    {
//        printf( "%u \n", val_bit_time[i] );
//    }
    
    /* Se busca el Bit Time con mayor frecuencia  */
    j = 0;
	for( i = 0; i < n_values_bit_time; i++ )
	{
		if( val_bit_time[ i ] > 0 )	/* Los valores a evaluar no deben ser cero */
		{
			if( valores_OK_bit_time[ j ][ 0 ] == val_bit_time[ i ] )  /* Comparamos si ya tenemos en base de datos el dato */
			{
				valores_OK_bit_time[ j ][ 1 ]++;		/* Si est�, incrementamos valor */
			}
			else        /* Si no esta el dato checamos si en la posici�n J hay contador */
			{
				if( valores_OK_bit_time[ j ][ 1 ] == 0 )  /* Si no hay conteo, guardamos el valor en posic�n de J e incrementamos valor */
				{
					valores_OK_bit_time[ j ][ 0 ] = val_bit_time[ i ];
					valores_OK_bit_time[ j ][ 1 ] ++ ;
				}
				else          /* Si hay valor, se incrementa J y guardamos ,siempre que j sea menor a 3 */
				{
					j++;
					if( j >= 3 ) break;
					valores_OK_bit_time[ j ][ 0 ] = val_bit_time[ i ];
					valores_OK_bit_time[ j ][ 1 ] ++ ;
				}
			}	
		}
	}
    
    printf("Bit Time 1: %u , Repeticiones: %u \n", valores_OK_bit_time[0][0], valores_OK_bit_time[0][1] );
	printf("Bit Time 2: %u , Repeticiones: %u \n", valores_OK_bit_time[1][0], valores_OK_bit_time[1][1] );
	printf("Bit Time 3: %u , Repeticiones: %u \n", valores_OK_bit_time[2][0], valores_OK_bit_time[2][1] );

	Bit_Time_max_ocurrencia = 0;
	/* Se elige el Bit Time con mayor ocurrencia */ 
	for( i = 0; i < 3  ; i++)
	{
		if( valores_OK_bit_time[i][1] > Bit_Time_max_ocurrencia )
		{
			Bit_Time_max_ocurrencia = valores_OK_bit_time[i][1];
			Bit_Time_max_index = i;
		}
	}

    /* Bit Time OK*/
    printf("Bit Time OK: %u\n", valores_OK_bit_time[Bit_Time_max_index][0] );
    
    /* Protecci�n contra divisi�n entre cero */
    if( 0 == valores_OK_bit_time[Bit_Time_max_index][0] ) valores_OK_bit_time[Bit_Time_max_index][0] = 1;  
    
    /* Valor de Baud Rate */
    Baud_Rate = ( uint32_t )( T23_TICK / ( valores_OK_bit_time[Bit_Time_max_index][0] * COUNT_TIME  ) );    
    printf("Baud Rate Detectado: %ubps \n", Baud_Rate );
    
//    /* Guardar dato de Buad Rate en NVM */
//    /**************************************************************************/
//    /* Se Inicializa Buffer */
//    for( i = 0; i < ( sizeof( databuff_Pagina1 ) / 4 ); i++ ) databuff_Pagina1[ i ] = 0;
//
//    /* Se guarda en databuff_Pagina1 los datos para NVM */
//    databuff_Pagina1[ 0 ] = MAGIC_WORD;
//    databuff_Pagina1[ 1 ] = Baud_Rate;
//    
//    /* Se borra la primera p�gina de NVM */
//    NVMErasePage( ( void * )NVM_PROGRAM_PAGE );
//    
//    /* Se guarda MAGIC WORD */
//    NVMWriteWord( ( void* )( NVM_PROGRAM_PAGE ), MAGIC_WORD );
//    
//    /* Se escribe 32 palabras empezando en la direcci�n del ROW NVM_PROGRAM_PAGE */
//    NVMWriteRow( ( void * )NVM_PROGRAM_PAGE, ( void * )databuff_Pagina1 );
//    
//    /* Verificar que los datos escritos en la memoria flash son validos */
//    if( memcmp( databuff_Pagina1, ( void * )NVM_PROGRAM_PAGE, sizeof( databuff_Pagina1 ) ) )
//    {
//        /* Si la informaci�n no concuerda, se manda mensaje */
//        printf("Error al guardar Datos en NVM... \n");
//    }
//    
//    /**************************************************************************/
  
    data_TimeBaudRate = ( float ) timerBaudRate/1000;
    printf("Tiempo de detecci�n: %4.3f seg \n", data_TimeBaudRate);    
    printf("Cambio de Velocidad de Logger de Salida de ArtLogSD a %ubps \n", Baud_Rate);
            
    /* Se cambia el baudaje al detectado */
    UARTSetDataRate( UARTx, PERIPHERAL_CLOCK, Baud_Rate );
    INTEnable( INT_SOURCE_UART_RX( UARTx ), INT_DISABLED );
    
    /* Liberaci�n de Memoria Din�mica */
    vPortFree( val_bruto_bit_time );
            
    /* Termina proceso de Auto Baud Rate */
    CloseCapture1();
    CloseTimer3();
    
    /* Se reconfigura el puerto Rx */
    /* INICIO Programacion de PPS */
    PPSUnLock;
    /* RS232 */
        PPSInput( 3, U1RX, RPB6 );
    /* FIN Programacion de PPS */
    PPSLock;
    
    /* Tasks Resume */
    vTaskResume( pxInitSDTask );
    vTaskResume( pxWriteSDTask );
    vTaskResume( pxCountFData );
    vTaskResume( pxFreeData );
    //vTaskResume( pxLedTask );
    vTaskResume( pxADCTask );    
 
    /* Estabilizaci�n por configuraci�n */
    delay_ms(100);      /* 0.1 seg */       

    /* Se guarda MENSAJE de Buad Rate*/  
    /* Armado de cadena de Baud Rate */      
    sprintf( texto_baud_rate, "%u", Baud_Rate );
    texto_baud_rate[ strlen( texto_baud_rate ) ] = 0x00;       /* Caracter NULL (\0) */
    
    /* Guardar datos en buad.txt */
    /**************************************************************************/
    accessmode3 = ( FA_CREATE_ALWAYS | FA_WRITE | FA_READ );
    FOpenRes = f_open( &file2, filename, ( uint8_t )accessmode3 );
    if ( FR_OK != FOpenRes ) f_open( &file2, filename, ( uint8_t )accessmode3 );
    
    FOpenRes = f_write(&file2, texto_baud_rate, strlen(texto_baud_rate), &numread);
    if ( FR_OK != FOpenRes ) f_write(&file2, texto_baud_rate, strlen(texto_baud_rate), &numread);
    
    FOpenRes = f_write(&file2, ",0", 2, &numread);
    if ( FR_OK != FOpenRes ) f_write(&file2, ",0", 2, &numread);  
    
    f_sync( &file2 );
    /**************************************************************************/
    
    /* Armado de cadena de Tiempo de Baud Rate */      
    sprintf( texto_time_baud_rate, "%4.3f", data_TimeBaudRate );
    texto_time_baud_rate[ strlen( texto_time_baud_rate ) ] = 0x00;       /* Caracter NULL (\0) */
    
    /* Concatenaci�n de cadenas */
    /* strcat( <cadena_destino>, <cadena_fuente> ) */
    strcat( mensaje_BaudRate_1, mensaje_BaudRate_2 );
    strcat( mensaje_BaudRate_1, texto_baud_rate );
    strcat( mensaje_BaudRate_1, mensaje_BaudRate_3 );
    strcat( mensaje_BaudRate_1, texto_time_baud_rate );
    strcat( mensaje_BaudRate_1, mensaje_BaudRate_4 );

    /* Env�o de cadena de datos para impresi�n de valor de Baud Rate */
    envio_msn_Buad_Rate = pvPortMalloc( strlen( mensaje_BaudRate_1 ) + 1 );
    memcpy( envio_msn_Buad_Rate,( uint8_t *)mensaje_BaudRate_1, strlen( mensaje_BaudRate_1 ) );
    envio_msn_Buad_Rate[ strlen( mensaje_BaudRate_1 ) ] = 0x00;      /* Caracter NULL (\0) */
    
    if (xQueueSendToBack( xRxedChars, &envio_msn_Buad_Rate, NOT_WAIT_TIME )== pdFALSE)
    {
        printf( "Error al Escribir en Queue [xRxedChars] ******** \n" );
        vPortFree( envio_msn_Buad_Rate );
    }
    
    /* Env�o de dato para estado de LEDs */
    dataStatusLED = 0x90;
    xQueueSendToBack(xStatusLED, &dataStatusLED, NOT_WAIT_TIME);
    
}
