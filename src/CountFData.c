/* Archivo:         CountFData.c
 * Autor:           Olaf I. G�mez P.
 * Fecha:           16/Diciembre/15
 * uC:              PIC32MX170F256B
 * Versi�n:         0
 * Descripci�n:     Archivo para Task "CountFData"
 *                    
 */

/* App Include */
#include "ArtLogSD.h"
#include "serial.h"

static volatile uint32_t Timer3 = 60000;

/* -------------------------------------------------------------------------- */
/* Nombre de la Funci�n:    TimeOutFreeD
 * Objetivo:                Timer para liberar datos
 * Observaciones:           */
void TimeOutFreeD( void *pvParameters )
{
    /* Evita warnings del compilador */
    (void) pvParameters;
    
    /* Definici�n de Variables */
    uint32_t n;

	n = Timer3;					
	if (n) Timer3 = --n;
    
    //printf( "%u\n", Timer3 );
    
}

/* -------------------------------------------------------------------------- */
/* Nombre de la Tarea:  CountFData
 * Objetivo:            Determina en que momento se debe Liberar la informaci�n 
 *                      de la interrupci�n que no se ha guardado
 * Observaciones:        */
void CountFData ( void *pvParameters )
{
    /* Solo para no tener warnings */
    ( void ) pvParameters;
    
    /* Definici�n de Variables  */
    uint8_t *datosC;
    
    /* Inicializaci�n de Variables */
    
    /* Limpiar semaphore */
    xSemaphoreTake( xFlagFreeData, NOT_WAIT_TIME ); 
    
    for( ;; )
    {
        /* Se tiene el mensaje */
        if( xSemaphoreTake( xFlagFreeData, NOT_WAIT_TIME) )
        {
            /* Contador de tiempo se reinicia */
            Timer3 = 2000;         /* 2 seg */
        } 
        
        if( Timer3 == 0 )
        {   
            printf("Timer3 = 0\n");
            /* Se asigna memoria din�mica para liberar datos */
            datosC = pvPortMalloc(iChar + 1);
            memcpy( datosC, (uint8_t  *) xRxedCharsPar, iChar);
            datosC[iChar] = 0;
            
            /* Se manda a Queue para su liberaci�n */
            if( xQueueSendToBack( xRxedCharsAux, &datosC, NOT_WAIT_TIME )== pdFALSE )
            {
                printf("[ CountFData  ] Error al Escribir en xRxedCharsAux ***************************************** \n");
                vPortFree(datosC);
            }
            
            /* Se limpian valores */
            iChar = 0;       
            
            /* Se reinicia conteo */
            Timer3 = 60000;         /* 1 min */
        }
    }
}