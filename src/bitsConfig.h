/* Archivo:         bitsConfig.h
 * Autor:           Olaf I. G�mez P.
 * Fecha:           28/Agosto/15
 * uC:              PIC32MX170F256B
 * Versi�n:         0
 * Descripci�n:     Bits de configuraci�n para ArtLogSD
 *                    
 */

#ifndef BITSCONFIG_H
#define	BITSCONFIG_H

/* Bits de Configuracion */ 
/* DEVCFG3 */
/* USERID = No Setting */ 
#pragma config PMDL1WAY  = ON       // Peripheral Module Disable Configuration  (Allow only one reconfiguration)
#pragma config IOL1WAY   = ON       // Peripheral Pin Select Configuration      (Allow only one reconfiguration)
#pragma config FUSBIDIO  = OFF      // USB USID Selection                       (Controlled by Port Function)
#pragma config FVBUSONIO = OFF      // USB VBUS OFF Selection                   (Controlled by Port Function)

// DEVCFG2
// Se configura para SYSCLK = 48 MHz ( (Posc/FPLLIDIV) * (FPLLMUL) ) / (FPLLODIV)
#pragma config FPLLIDIV  = DIV_2    // PLL Input Divider                        (2x Divider)
#pragma config FPLLMUL   = MUL_24   // PLL Multiplier                           (24x Multiplier)
#pragma config FPLLODIV  = DIV_2    // System PLL Output Clock Divider          (PLL Divide by 2)

// DEVCFG1
#pragma config FNOSC = PRIPLL       // Oscillator Selection Bits                (Primary Osc w/PLL (XT+PLL,HS+PLL,EC+PLL))
#pragma config FSOSCEN = ON         // Secondary Oscillator Enable              (Enabled)
#pragma config IESO = OFF           // Internal/External Switch Over            (Disabled)
#pragma config POSCMOD = HS         // Primary Oscillator Configuration         (HS osc mode)
#pragma config OSCIOFNC = OFF       // CLKO Output Signal Active on the OSCO Pin(Disabled)
#pragma config FPBDIV = DIV_2       // Peripheral Clock Divisor                 (Pb_Clk is Sys_Clk/2)
#pragma config FCKSM = CSDCMD       // Clock Switching and Monitor Selection    (Clock Switch Disable, FSCM Disabled)
#pragma config WDTPS = PS1          // Watchdog Timer Postscaler                (1:1)
#pragma config WINDIS = OFF         // Watchdog Timer Window Enable             (Watchdog Timer is in Non-Window Mode)
#pragma config FWDTEN = OFF         // Watchdog Timer Enable                    (WDT Disabled (SWDTEN Bit Controls))
#pragma config FWDTWINSZ = WINSZ_25 // Watchdog Timer Window Size               (Window Size is 25%)

// DEVCFG0
#pragma config JTAGEN = OFF         // JTAG Enable                              (JTAG Disabled)
#pragma config ICESEL = ICS_PGx1    // ICE/ICD Comm Channel Select              (Communicate on PGEC1/PGED1)
#pragma config PWP = OFF            // Program Flash Write Protect              (Disable)
#pragma config BWP = OFF            // Boot Flash Write Protect bit             (Protection Disabled)
#pragma config CP = OFF             // Code Protect                             (Protection Disabled)

#endif	/* BITSCONFIG_H */

