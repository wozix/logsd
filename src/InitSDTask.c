/* Archivo:         InitSDTask.c
 * Autor:           Olaf I. G�mez P.
 * Fecha:           16/Diciembre/15
 * uC:              PIC32MX170F256B
 * Versi�n:         0
 * Descripci�n:     Archivo para Task "InitSDTask"
 *                    
 */

/* App Include */
#include "ArtLogSD.h"
#include "memoryCard.h"
#include "serial.h"
#include "rtBDebug.h"

static volatile uint32_t timerCD_SW_1;

/* Fecha de creaci�n de archivos */
volatile WORD rtcYear = 2015;
volatile BYTE rtcMon = 12, rtcMday = 16, rtcHour, rtcMin, rtcSec;

/* -------------------------------------------------------------------------- */
/* Nombre de Funci�n:   open_append
 * Objetivo:            Abre o crea un archivo en "append mode". */
FRESULT open_append (
    FIL* fp,            /* [OUT] File object to create */
    const char* path    /* [IN]  File name to be opened */
)
{
    FRESULT fr;

    /* Opens an existing file. If not exist, creates a new file. */
    fr = f_open(fp, path, FA_WRITE | FA_OPEN_ALWAYS);
    if (fr == FR_OK) {
        /* Seek to end of the file to append data */
        fr = f_lseek(fp, f_size(fp));
        printf("[ open_append ] Size File: %u \n",f_size(fp));
        
        /* Se evalua el tama�o del archivo */
        if( maxSizeFile <= f_size(fp) )
            fr = FR_WRITE_PROTECTED; 
        
        if (fr != FR_OK)
            f_close(fp);
    }
    return fr;
}

/* -------------------------------------------------------------------------- */
/* Nombre de Funci�n:   put_rc
 * Objetivo:            Monitor de resultados de las funciones FatFs. */
static void put_rc (FRESULT rc)
{
	const char *str =
		"OK\0" "DISK_ERR\0" "INT_ERR\0" "NOT_READY\0" "NO_FILE\0" "NO_PATH\0"
		"INVALID_NAME\0" "DENIED\0" "EXIST\0" "INVALID_OBJECT\0" "WRITE_PROTECTED\0"
		"INVALID_DRIVE\0" "NOT_ENABLED\0" "NO_FILE_SYSTEM\0" "MKFS_ABORTED\0" "TIMEOUT\0"
		"LOCKED\0" "NOT_ENOUGH_CORE\0" "TOO_MANY_OPEN_FILES\0";
	FRESULT i;

	for (i = 0; i != rc && *str; i++) 
    {
		while (*str++) ;
	}
    printf("rc=%u FR_%s\n", (UINT)rc, str);
}

/* -------------------------------------------------------------------------- */
/* Nombre de la Tarea:  LedTask
 * Objetivo:            Busca e Inicializa la memor�a SD 
 * Observaciones:       Esta tarea no dejara entrar a las otras hasta que se 
 *                      inicialize la memoria. */
void InitSDTask ( void *pvParameters )
{
    /* Solo para no tener warnings */
    ( void ) pvParameters;
    
    /* Definiciones */
    uint8_t dataStatusLED;
    uint8_t SD_Card;
    uint8_t numtry;
    bool flagOK;
    DSTATUS resF;
     
    /* Inicilizaci�n de Variables */
    dataStatusLED = 0x80;
            
    for( ;; )
    {            
        /* Se captura la presencia/ausencia de la Memoria SD */
        if( !CD_SW )
        {
            /* Se inicializa contador para eliminar rebotes de Switch */
            timerCD_SW_1 = 1000;           /* 1 seg */
            do{Nop();}
            while( timerCD_SW_1 );
            
            /* Se evalua de nueva cuenta el estado de CD_SW para determinar el valor de SD_Card */
            if( !CD_SW ) SD_Card = 1;
            else SD_Card = 0; 
        }
        else 
        {
            /* Se inicializa contador para eliminar rebotes de Switch */
            timerCD_SW_1 = 1000;           /* 1 seg */
            do{Nop();}
            while( timerCD_SW_1 );
            
            /* Se evalua de nueva cuenta el estado de CD_SW para determinar el valor de SD_Card */
            if( CD_SW ) SD_Card = 0;
            else SD_Card = 1;
        }
        
        /* Se evalua para inicializar memoria */
        if( SD_Card == 1 )
        {
            
            /* L�nea para Run-Time Behavior */
            if( 1 == LABEL_RTB_DEBUG ) rtB_debug( INITSD_ON );
            
            /* Mensaje indicando que la SD Card esta presente*/
            printf( "[ InitSDTask  ] SD Card: OK...\n" );
            
            /* Inicializaci�n de SD */
            printf( "[ InitSDTask  ] Inicializando Disco...\n" );
            numtry = 0;
            flagOK = false;
            
            do
            {
                numtry++;
                printf( "[ InitSDTask  ] Intento %u: ", numtry );
                resF = disk_initialize( (BYTE)0 );
                put_rc( resF );       /* Unidad L�gica 0 */        
                if( !resF ) flagOK = true;
            } while( numtry < 5 && resF );
            
            if( flagOK == true )
            {
                /* Actualizaci�n de estado de LEDs */
                dataStatusLED |= 0x20;
                
                /* Forzar la inicializacion de la unidad logica (Montar la unidad) */
                printf( "[ InitSDTask  ] Montando la unidad Logica...\n" );
                numtry = 0;
                flagOK = false;
            
                do
                {
                    numtry++;
                    printf( "[ InitSDTask  ] Intento %u: ", numtry );
                    resF = f_mount( &FatFs, "", (BYTE)0 );
                    put_rc( resF );       /* Unidad L�gica 0 */        
                    if( !resF ) flagOK = true;
                } while( numtry < 5 && resF );
                
                if( flagOK == true )
                {
                    /* Actualizaci�n de estado de LEDs */
                    dataStatusLED |= 0x10;
                    
                    /* Checar espacio libre en SD Card */
                    valFreeS = GetFreeSpace();
                    if( valFreeS <= minSpaceSDCard  )
                    {
                        printf( "[ InitSDTask  ] Memoria SD Card Llena...\n" );
                        printf( "[ InitSDTask  ] Vaciar Memoria SD Card...\n" );

                        /* Deshabilitaci�n de Interrupci�n para evitar recibir datos por UART */
                        INTEnable( INT_SOURCE_UART_RX( UARTx ), INT_DISABLED );

                        /* Unmount Filesystem */
                        f_mount((void *)0,"",0);
                        
                        /* L�nea para Run-Time Behavior */
                        if( 1 == LABEL_RTB_DEBUG ) rtB_debug( INITSD_OFF );
                        
                        /* Se debe suspender la tarea, por memoria llena */
                        vTaskSuspend( NULL );  
                    }
                    else
                    {                        
                        /* Se realiza la busqueda del �ltimo archivo para guardar datos */
                        BusqArchivo();
                        
                        /* Habilitaci�n de Interrupci�n para recibir datos por UART */
                        INTEnable( INT_SOURCE_UART_RX( UARTx ), INT_ENABLED );
                                                
                        /* Habilitar interrupciones por UART */
                        INTConfigureSystem( INT_SYSTEM_CONFIG_MULT_VECTOR );    /* Configuraci�n en modo multivector */

                        /* Habilitaci�n de Interrupciones */
                        INTEnableInterrupts(); 

                        /* Se envia estado de LEDs */
                        xQueueSendToBack( xStatusLED, &dataStatusLED, NOT_WAIT_TIME );
                        
                        /* L�nea para Run-Time Behavior */
                        if( 1 == LABEL_RTB_DEBUG ) rtB_debug( INITSD_OFF );
                        
                        /* Se debe suspender la tarea, solo se debe inicializar la memoria al aranque */
                        vTaskSuspend( NULL );   
                    }
                }
                else
                {
                    /* NO se puede montar la unidad l�gica */
                    printf( "[ InitSDTask  ] No se puede montar la unidad l�gica...\n" );
                    printf( "[ InitSDTask  ] Cambiar o formatear memoria...\n" );
                    
                    /* Se envia estado de LEDs */
                    xQueueSendToBack( xStatusLED, &dataStatusLED, NOT_WAIT_TIME );
                    
                    /* L�nea para Run-Time Behavior */
                    if( 1 == LABEL_RTB_DEBUG ) rtB_debug( INITSD_OFF );
                    
                    /* Se suspende la tarea */
                    vTaskSuspend( NULL );
                }
            }
            else
            {
                /* NO se puede inicializar memoria */
                printf( "[ InitSDTask  ] No se puede inicializar memoria...\n" );
                printf( "[ InitSDTask  ] Cambiar o formatear memoria...\n" );
                
                /* Se envia estado de LEDs */
                xQueueSendToBack( xStatusLED, &dataStatusLED, NOT_WAIT_TIME );
                
                /* L�nea para Run-Time Behavior */
                if( 1 == LABEL_RTB_DEBUG ) rtB_debug( INITSD_OFF );
                
                /* Se suspende la tarea */
                vTaskSuspend( NULL );
            }
        }
        else
        {
            /* Mensaje indicando que la SD Card no esta presente*/
            printf( "[ InitSDTask  ] NO hay SD Card...\n" );
            
            /* Tiempo de Espera */
            vTaskDelay(WAIT_TIME_2_seg);
        }  
    }
}

/* -------------------------------------------------------------------------- */
/* Nombre de Funci�n:   BusqArchivo
 * Objetivo:            Funci�n que busca el ultimo archivo donde guardar datos . */
void BusqArchivo( void )
{
    /* Definici�n de variables */
    char filename[]= "LGSD0000.txt";
    uint8_t flagLastFile;
    uint8_t flag2x;
    uint32_t Buffp;
    uint32_t File_Past;
    uint32_t File_Present;
    uint32_t File_New;

    /* Variables de FatFs */
    FRESULT fr;
    FILINFO fno;
    FRESULT FOpenRes;
    uint32_t accessmode2;
    
    /* Inicializaci�n de variables */
    flagLastFile = 0;   /* Bandera para indicar que el ultimo archivo fue encontrado */
    File_Past = 0;
    File_Present = 0;
    File_New = 0;
    flag2x = 1;
    accessmode2 = (FA_CREATE_ALWAYS | FA_OPEN_ALWAYS | FA_CREATE_NEW | FA_WRITE |FA_READ);

    Buffp = 0;
    
    /* Se realiza la busqueda del ultimo Archivo creado */
    while( flagLastFile == 0 )
    {
        /* Archivo en busqueda */    
        printf("[ BusqArchivo ] Prueba para ultimo Archivo [%s]...\n",filename);
    
        /* Funcion de busqueda de archivos de FatFs */
        fr = f_stat(filename, &fno);    
        
        switch (fr) 
        {
            case FR_OK:
                /* El Archivo existe y se evalua el tama�o*/
                printf("[ BusqArchivo ] Size: %u\n", fno.fsize);
                if( fno.fsize < maxSizeFile )
                {
                    /* Se encontro el ultimo Archivo con espacio disponible para almacenar datos */
                    flagLastFile = 1;           /* Bandera para salir del while */
                }
                else
                {
                    /* Se encontro el archivo y no tiene espacio para almacenar informacion 
                    se aumenta el nombre para la busqueda del ultimo archivo */

                    /* Se incrementa indice (Buffp) de archivo para*/
                    if( Buffp <= 1 )
                    {
                        File_Past = Buffp;      /* Se actualiza archivo Past*/
                        Buffp++;
                        File_Present = Buffp;   /* Se actualiza archivo Present*/
                    }
                    else if( (Buffp >= 2 & Buffp <= 4096) & (flag2x == 1) ) 
                    {
                        /* Busqueda en multiplos de 2 */
                        File_Past = Buffp;      /* Se actualiza archivo Past*/
                        Buffp = Buffp << 1;
                        File_Present = Buffp;   /* Se actualiza archivo Present*/
                    }
                    else if( flag2x == 0 )
                    {
                        /* Busqueda entre dos multiplos de 2 */
                        File_Past = File_New;   /* Se actualiza archivo Past*/
                        
                        /* Evita error en busqueda cuando hay una diferencia entre 
                       archivos de 1 */
                        if( File_Present - File_Past == 1 )
                        {
                            /* Se encontro el ultimo Archivo */
                            flagLastFile = 1;
                            break;
                        }
                        
                        /* Se calcula nuevo nombre de archivo entre dos intervalos */    
                        Buffp = File_Past + ((File_Present - File_Past)/2);
                        File_New = Buffp;   /* Se actualiza archivo New*/
                    }

                    /* Se llama a la funcion para nuevo nombre de archivo */
                    newNameFile( Buffp, filename );
                }
                break;

            case FR_NO_FILE:
                        
                printf("[ BusqArchivo ] El Archivo no existe.\n");
                /* Si File_Past == File_Present caso cuando Buffp = 0 
                   Si File_Present - File_Past = 1 caso cuando Buffp >= 1 */
                if( (File_Past == File_Present) | (File_Present - File_Past == 1) )
                {
                    /* Se encontro el ultimo Archivo */
                    flagLastFile = 1;
                    break;
                }
                else
                {
                    /* Se actualiza File_Present */
                    if( File_New > 0 )
                    {
                        File_Present = File_New; 
                    }
                      
                    /* Se limpia bandera para modificar el calculo del nuevo nombre de
                       busqueda del archivo entre dos archivos multiplos de dos ejemplo:
                     * Busqueda entre LGSD0004.txt y LGSD0008.txt */    
                    flag2x = 0;
                    
                    /* Evita error en busqueda cuando hay una diferencia entre 
                       archivos de 1 */
                    if( File_Present - File_Past == 1 )
                    {
                        /* Se encontro el ultimo Archivo */
                        flagLastFile = 1;
                        break;
                    }
                            
                    /* Se calcula nuevo nombre de archivo entre dos intervalos */    
                    Buffp = File_Past + ((File_Present - File_Past)/2);
                    File_New = Buffp;   
                    
                    /* Se llama a la funcion para nuevo nombre de archivo */
                    newNameFile( Buffp, filename );

                    break;
                }

            default:
                printf("[ BusqArchivo ] Error: ");
                put_rc(fr);
                printf("\n");
        }
    }
    
    /* Se verifica si existe el archivo, si es el caso abre el archivo y
    * si es menor de maxSizeFile lo usa para seguir escribiendo. Si no existe lo crea */
            
    FOpenRes = open_append (&file1, filename);
            
    if( FOpenRes == FR_OK )
    {
        printf("[ BusqArchivo ] Archivo existente: %s \n", filename);
    }
    else
    {
        /* Se incrementa indice de archivo */
        Buffp++;
        /* Se llama a la funcion para nuevo nombre de archivo */
        newNameFile( Buffp, filename );

        FOpenRes = f_open (&file1, filename, (BYTE)accessmode2);
        printf("[ BusqArchivo ] Archivo Creado: %s \n", filename);
    }
    
    /* Se almacena ultimo valor de Buffp */
    countFile = Buffp;
    
}

/* -------------------------------------------------------------------------- */
/* Nombre de Funci�n:   newNameFile
 * Objetivo:            Funci�n que genera el nuevo nombre de archivo . */
void newNameFile( uint32_t Buffp, char file2name[] )
{
    /* Declaraci�n de Variables */
    
    /* Se inicializa a LGSD"0000" el nombre del archivo */
    file2name[0]  = 'L';
    file2name[1]  = 'G';
    file2name[2]  = 'S';
    file2name[3]  = 'D';
    file2name[4]  = 48;
    file2name[5]  = 48;
    file2name[6]  = 48;
    file2name[7]  = 48;
    file2name[8]  = '.';
    file2name[9]  = 't';
    file2name[10] = 'x';
    file2name[11] = 't';

    /* Se determina el nuevo nombre de archivo de acuerdo al �ndice */
    if ( Buffp <= 9 )
    {
        file2name[7] = Buffp + 48;
    }
    else if ( Buffp <= 99 )
    {
        file2name[6] = Buffp / 10 + 48;
        file2name[7] = Buffp % 10 + 48;
    }
    else if( Buffp <= 999 )
    {
        file2name[5] = Buffp / 100 + 48;
        file2name[6] = (Buffp % 100) / 10 + 48;
        file2name[7] = (Buffp % 100) % 10 + 48;
    }        
    else if( Buffp <= 9999 )
    {
        file2name[4] = Buffp / 1000 + 48;
        file2name[5] = (Buffp % 1000) / 100 + 48;
        file2name[6] = ((Buffp % 1000) % 100) / 10 + 48;
        file2name[7] = ((Buffp % 1000) % 100) % 10 + 48;
    }
}

/* -------------------------------------------------------------------------- */
/* Nombre de la Funci�n:    GetFreeSpace
 * Objetivo:                Obtiene el espacio libre en la SD Card
 * Observaciones:           */
DWORD GetFreeSpace( void )
{    
    /* Definici�n de Variables */
    FRESULT GFreeRes;
    DWORD fre_clust, fre_sect, tot_sect; 
    FATFS *fs;
        
    /* Determinar espacio libre en SD Card */
    GFreeRes = f_getfree("0:",&fre_clust, &fs );

    /* Obtener total de sectores y sectores libres */
    tot_sect = (fs->n_fatent - 2) * fs->csize;
    fre_sect = fre_clust * fs->csize;

    /* Imprimir espacio libre (se asume 512 bytes/sector) */
    //printf("[ GetFreeSpace] %10lu KB Espacio Total de la unidad\n",tot_sect / 2);
    //printf("[ GetFreeSpace] %10lu KB Espacio Disponible\n", fre_sect / 2); 
    
    fre_sect = fre_sect/2;
    return fre_sect;
}

/* -------------------------------------------------------------------------- */
/* Nombre de la Funci�n:    TimeOutCD_SW
 * Objetivo:                Timer para eliminar rebotes 
 * Observaciones:           */
void TimeOutCD_SW_1( void *pvParameters )
{
    /* Evita warnings del compilador */
    (void) pvParameters;
    
    /* Definici�n de Variables */
    uint32_t n;
    
    n = timerCD_SW_1;
    if (n) timerCD_SW_1 = --n;
}

/*---------------------------------------------------------*/
/* User Provided RTC Function for FatFs module             */
/*---------------------------------------------------------*/
/* This is a real time clock service to be called from     */
/* FatFs module. Any valid time must be returned even if   */
/* the system does not support an RTC.                     */
/* This function is not required in read-only cfg.         */
DWORD get_fattime (void)
{
	DWORD tmr;
	//_DI();
	/* Pack date and time into a DWORD variable */
	tmr =	  (((DWORD)rtcYear - 1980) << 25)
			| ((DWORD)rtcMon << 21)
			| ((DWORD)rtcMday << 16)
			| (WORD)(rtcHour << 11)
			| (WORD)(rtcMin << 5)
			| (WORD)(rtcSec >> 1);
	//_EI();

	return tmr;
}