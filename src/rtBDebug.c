/* Archivo:         rtBDebug.c
 * Autor:           Olaf I. G�mez P.
 * Fecha:           29/Enero/16
 * uC:              PIC32MX170F256B
 * Versi�n:         0
 * Descripci�n:     Rutina para Run-Time Behavior Debug
 *                    
 */

/* Librer�as */
#include "rtBDebug.h"

void rtB_debug( uint8_t data )
{
    uint16_t temp;
    uint8_t i;
    
    temp = 0x200 + (data << 1);
    for (i = 0; i < 10; i++) 
    {
        LATBbits.LATB9 = (temp & 0x001);
        temp >>= 1;
                        /* 6.80 MBps */
        Nop();          /* 5.35 MBps */
        Nop();          /* 4.75 MBps */
        Nop();          /* 4.35 MBps */
        Nop();          /* 4.00 MBps */
        Nop();          /* 3.70 MBps */
        Nop();          /* 3.40 MBps */
        Nop();          /* 3.20 MBps */
        Nop();          /* 3.00 MBps */
        Nop();          /* 2.65 MBps */
        Nop();          /* 2.525 MBps */
        Nop();          /* 2.40 MBps */
        Nop();          /* 2.29 MBps */
        Nop();          /* 2.18 MBps */
        Nop();          /* 2.08 MBps */
        Nop();          /* 2.00 MBps */ 
    }
}