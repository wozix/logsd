/* Archivo:         Serial.h
 * Autor:           Olaf I. G�mez P.
 * Fecha:           22/Octubre/15
 * uC:              PIC32MX170F256B
 * Versi�n:         0
 * Descripci�n:     Configuraci�n de RS232, Interrupciones, Task, etc.
 *                    
 */

#ifndef SERIAL_H
#define	SERIAL_H

/* Includes */
#include <stdbool.h>
#include <plib.h>
#include <xc.h>

/* "Defines" para configuraci�n de UART */
#define config_flags                ( UART_ENABLE_PINS_TX_RX_ONLY | UART_ENABLE_HIGH_SPEED )
#define config_FIFO_Mode            ( UART_INTERRUPT_ON_TX_NOT_FULL | UART_INTERRUPT_ON_RX_NOT_EMPTY )
#define config_Line_Control_Mode    ( UART_DATA_SIZE_8_BITS | UART_PARITY_NONE | UART_STOP_BITS_1 )

/* Baud rate */
#define COM_BAUD_RATE               ( 500000 )

/* N�mero de UART */
#define UARTx                       UART1
#define _UARTx_VECTOR               _UART1_VECTOR
#define INT_UxRX                    INT_U1RX
#define INT_UxTX                    INT_U1TX

/* La secuencia transmitida debe terminar con los caracteres <LF> <CR> */
#define serLAST_BYTE_LF             ( 0x0A )  /* Salto de linea <LF> */
#define serLAST_BYTE_CR             ( 0x0D )  /* Retorno de carro <CR> */

/* Configuraci�n de UART  */
void vSerialPortInit ( void );

/* Declaracion de Pending Function para Queue */
void vProcessQueue ( void  *pvParameter1, uint32_t ulParameter2 );

/* Declaracion de Pending Function para envio de mensaje de Auto Baud Rate */
void vProcessMensajeBaudRate( void  *pvParameter1, uint32_t ulParameter2 ); 

#endif	/* SERIAL_H */

